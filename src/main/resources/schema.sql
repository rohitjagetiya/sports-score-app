CREATE TABLE IF NOT EXISTS otp (
	id SERIAL NOT NULL PRIMARY KEY,
	userid VARCHAR(200) NOT NULL,
	useremail VARCHAR(1000) NOT NULL,
	otp VARCHAR(6) NOT NULL,
	date TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS sportsuser (
	id SERIAL NOT NULL PRIMARY KEY,
	userid VARCHAR(200) NOT NULL,
	useremail VARCHAR(1000) NOT NULL,
	username VARCHAR(250) NOT NULL,
	password VARCHAR(100) NOT NULL,
	blacklisted boolean DEFAULT false,
	profile VARCHAR(500)
);

CREATE TABLE IF NOT EXISTS team (
	id SERIAL NOT NULL PRIMARY KEY,
	teamname VARCHAR(200) NOT NULL,
	sport VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS teamplayer (
	id SERIAL NOT NULL PRIMARY KEY,
	teamid BIGINT NOT NULL,
	playerid BIGINT NOT NULL,
	playername VARCHAR(200) NOT NULL
);

CREATE TABLE IF NOT EXISTS player (
	id SERIAL NOT NULL PRIMARY KEY,
	name VARCHAR(200) NOT NULL,
	age BIGINT,
	gender VARCHAR(1),
	bio VARCHAR(2000),
	dob DATE,
	sport VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS fixture (
	id SERIAL NOT NULL PRIMARY KEY,
	firstteam VARCHAR(200) NOT NULL,
	secondteam VARCHAR(200) NOT NULL,
	name VARCHAR(500) NOT NULL,
	matchtype VARCHAR(100),
	venue VARCHAR(300) NOT NULL,
	stadium VARCHAR(300) NOT NULL,
	date DATE NOT NULL,
	TIME TIME NOT NULL,
	seriesid BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS series (
	id SERIAL NOT NULL PRIMARY KEY,
	name VARCHAR(300) NOT NULL,
	sport VARCHAR(100) NOT NULL,
	venue VARCHAR(300) NOT NULL,
	startdate DATE NOT NULL,
	enddate DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS blog (
	id SERIAL NOT NULL PRIMARY KEY,
	fixtureid BIGINT NOT NULL,
	userid VARCHAR(100) NOT NULL,
	usercomments VARCHAR(2000) NOT NULL
);

CREATE TABLE IF NOT EXISTS newsfeed (
	id SERIAL NOT NULL PRIMARY KEY,
	title VARCHAR(800) NOT NULL,
	article VARCHAR(4000) NOT NULL,
	username VARCHAR(2000) NOT NULL,
	posteddate TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS cricket_live (
	id SERIAL NOT NULL PRIMARY KEY,
	fixtureid BIGINT NOT NULL,
	score BIGINT,
	wickets BIGINT,
	name VARCHAR(500) NOT NULL,
	venue VARCHAR(300) NOT NULL,
	stadium VARCHAR(300) NOT NULL,
	secondteam VARCHAR(200) NOT NULL,
	firstteam VARCHAR(200) NOT NULL,
	firstbatsman VARCHAR(200),
	secondbatsman VARCHAR(200),
	firstbowler VARCHAR(200),
	secondbowler VARCHAR(200),
	firstbowlerfigures VARCHAR(200),
	secondbowlerfigures VARCHAR(200),
	firstbatsmanscore BIGINT,
	secondbatsmanscore BIGINT,
	currentover VARCHAR(10),
	totalovers VARCHAR(10),
	batsmanonstrike VARCHAR(100), 
	toss VARCHAR(200),
	teambattingfirst VARCHAR(200),
	teambattingsecond VARCHAR(200),
	format varchar(100),
	date DATE NOT NULL,
	TIME TIME NOT NULL,
	matchstatus VARCHAR(100) NOT NULL,
	secondteamscore BIGINT,
	secondteamwickets BIGINT,
	matchresult VARCHAR(300),
	secondteamcurrentover VARCHAR(10),
	secondteamtotalovers VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS football_live (
	id SERIAL NOT NULL PRIMARY KEY,
	fixtureid BIGINT NOT NULL,
	name VARCHAR(500) NOT NULL,
	venue VARCHAR(300) NOT NULL,
	stadium VARCHAR(300) NOT NULL,
	secondteam VARCHAR(200) NOT NULL,
	firstteam VARCHAR(200) NOT NULL,
	firstteamscore BIGINT,
	secondteamscore BIGINT,
	firstteamgoals VARCHAR(1000),
	secondteamgoals VARCHAR(1000),
	date DATE NOT NULL,
	TIME TIME NOT NULL,
	matchstatus VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS adminuser (
	id SERIAL NOT NULL,
	name VARCHAR(15) NOT NULL,
	password VARCHAR(15) NOT NULL,
	CONSTRAINT name UNIQUE (name)
);
