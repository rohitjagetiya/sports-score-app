$(document).ready(function() {
	alertify.set('notifier','position', 'top-right');
	$("footer").css("position", "absolute");
	$(document).bind('keypress', function(e) {
        if(e.keyCode==13){
        	 login();
         }
    });
});

function login() {
	if ($('#adminloginname').val() == "" || $('#adminloginname').val() == null
			|| $('#adminloginname').val() === undefined) {
		alertify.error("Please enter your name!");
		return false;
	}
	if ($('#adminloginpassword').val() == "" || $('#adminloginpassword').val() == null
			|| $('#adminloginpassword').val() === undefined) {
		alertify.error("Please enter your password!");
		return false;
	}
	event.preventDefault();
	var user = {
			"name" : $('#adminloginname').val(),
			"password" : $('#adminloginpassword').val()
		};
	var token = make_base_auth($('#adminloginname').val(),$('#adminloginpassword').val());
	$('#adminlogin').prop('disabled', true);
	$('#adminlogin').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> loging in....please wait!');
	$.ajax({
		url : '/zmt/hdd/authenticateuser',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(user),
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val(),
		    'Authorization': token
		},
		processData : false,
		success : function(data, textStatus, jQxhr) {
			$('#adminlogin').prop('disabled', false);
			$('#adminlogin').html('LOGIN');
			if(data.responseCode == "OK"){
				localStorage['token'] = token;
				localStorage['name'] = $('#adminloginname').val();
				localStorage['count'] = 0;
				window.location.href = "/admindashboard";
			}else{
				alertify.error("Invalid credentials!");
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			$('#adminlogin').html('LOGIN');
			$('#adminlogin').prop('disabled', false);
			alertify.error("Invalid credentials!");
		}
	});
}

function make_base_auth(user, password) {
	  var tok = user + ':' + password;
	  var hash = Base64.encode(tok);
	  return "Basic " + hash;
	}

/**
 * Form Validation MDB-Bootstrap
 * 
 * @returns
 */
(function() {
	'use strict';
	window.addEventListener('load', function() {
		var forms = document.getElementsByClassName('needs-validation');
		Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();