$(document).ready(function() {
	
});
var playerid;
function createCricketTeam(){
	$('#createcricketteambtn').prop('disabled', true);
	$('#createcricketteambtn').html('creating');
	var team ={
		"name": $('#teamname').val(),
		"sport": "cricket"
	}
	$.ajax({
		url : '/zmt/hdd/createteam',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(team),
		processData : false,
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode != "OK"){
				alertify.error(data.errorMessage);
				$('#createcricketteambtn').prop('disabled', false);
				$('#createcricketteambtn').html('create');
				return false;
			}else{
				alertify.success("Created Team!");
				$('#createcricketteambtn').prop('disabled', false);
				$('#createcricketteambtn').html('create');
				$("#teamname").val("");
				location.reload();
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error(errorThrown);
			$('#cricketlivescoreupdatebtn').prop('disabled', false);
			$('#cricketlivescoreupdatebtn').html('UPDATE');
			return false;
		}
	});
}

function createCricketSeries(){
	$('#createcricketseriesbtn').prop('disabled', true);
	$('#createcricketseriesbtn').html('creating');
	var team ={
		"name": $('#seriesname').val(),
		"sport": "cricket",
		"venue": $('#seriesvenue').val(),
		"startDate": $('#seriesstartdate').val(),
		"endDate": $('#seriesenddate').val()
	}
	$.ajax({
		url : '/zmt/hdd/createseries',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(team),
		processData : false,
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode != "OK"){
				alertify.error(data.errorMessage);
				$('#createcricketseriesbtn').prop('disabled', false);
				$('#createcricketseriesbtn').html('create');
				return false;
			}else{
				alertify.success("Created Series!");
				$('#createcricketseriesbtn').prop('disabled', false);
				$('#createcricketseriesbtn').html('create');
				location.reload();
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error(errorThrown);
			$('#cricketlivescoreupdatebtn').prop('disabled', false);
			$('#cricketlivescoreupdatebtn').html('create');
			return false;
		}
	});
}

function createCricketFixture(){
	$('#createcricketfixturebtn').prop('disabled', true);
	$('#createcricketfixturebtn').html('creating');
	var team ={
		"name": $('#fixturename').val(),
		"sport": "cricket",
		"venue": $('#fixturevenue').val(),
		"startDate": $('#fixturestartdate').val(),
		"matchType": $('#fixturematchformat').val(),
		"series": $('#seriesselect').val(),
		"firstTeam" : $('#fixturefirstteam').val(),
		"secondTeam" : $('#fixturesecondteam').val(),
		"stadium" : $('#fixturestadium').val(),
		"date" : $('#fixturestartdate').val(),
		"time" : $('#fixturematchtime').val()
	}
	$.ajax({
		url : '/zmt/hdd/createfixture',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(team),
		processData : false,
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode != "OK"){
				alertify.error(data.errorMessage);
				$('#createcricketfixturebtn').prop('disabled', false);
				$('#createcricketfixturebtn').html('create');
				return false;
			}else{
				alertify.success("Created Fixture!");
				$('#createcricketseriesbtn').prop('disabled', false);
				$('#createcricketseriesbtn').html('create');
				location.reload();
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error(errorThrown);
			$('#createcricketfixturebtn').prop('disabled', false);
			$('#createcricketfixturebtn').html('create');
			return false;
		}
	});
}

function createcricketplayer(){
	$('#createplayerbtn').prop('disabled', true);
	$('#createplayerbtn').html('creating');
	var team ={
		"teamId": $('#teamid').val(),
		"playerName": $('#playername').val()
	}
	$.ajax({
		url : '/zmt/hdd/createplayer',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(team),
		processData : false,
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode != "OK"){
				alertify.error(data.errorMessage);
				$('#createplayerbtn').prop('disabled', false);
				$('#createplayerbtn').html('create');
				return false;
			}else{
				$('#createplayerbtn').prop('disabled', false);
				$('#createplayerbtn').html('create');
				$("#playername").val("");
				alertify.success("Created Player!");
				location.reload();
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error(errorThrown);
			$('#createplayerbtn').prop('disabled', false);
			$('#createplayerbtn').html('create');
			return false;
		}
	});
}

function delcricketplayer(id){
	$.ajax({
		url : '/zmt/hdd/deleteplayer/' + id,
		type : 'delete',
		success : function(data, textStatus, jQxhr) {
			alertify.success("Player Deleted!");
			location.reload(true);
		},
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		error : function(jqXhr, textStatus, errorThrown) {
			return false;
		}
	});
}

function setplayerid(id){
	playerid = id;
}

function updatecricketplayer(){
	$.ajax({
		url : '/zmt/hdd/updateplayer/' + playerid + '/' + $('#updateplayername').val(),
		type : 'post',
		success : function(data, textStatus, jQxhr) {
			alertify.success("Player Updated!");
			location.reload(true);
		},
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error("Could not add Player!");
			return false;
		}
	});
}

function updateteam(){
	if($('#updateteamname').val() == ""){
		alertify.error("Please enter team name");
	}
	$.ajax({
		url : '/zmt/hdd/updateteam/' + $('#teamid').val() + '/' + $('#updateteamname').val(),
		type : 'post',
		success : function(data, textStatus, jQxhr) {
			alertify.success("Team Name Updated!");
			window.location.href = "/admincricketteams/" + $('#updateteamname').val();
		},
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error("Could not updateTeam!");
			return false;
		}
	});
}

function delteam(){
	$.ajax({
		url : '/zmt/hdd/deleteteam/' + $('#teamid').val(),
		type : 'delete',
		success : function(data, textStatus, jQxhr) {
			alertify.success("Team Deleted!");
			window.location.href = "/admindashboard";
		},
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		error : function(jqXhr, textStatus, errorThrown) {
			return false;
		}
	});
}