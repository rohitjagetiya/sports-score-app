$(document).ready(function() {
	
});

function delfixture(id){
	$.ajax({
		url : '/zmt/hdd/deletefixture/' + id,
		type : 'delete',
		success : function(data, textStatus, jQxhr) {
			alertify.success("Fixture Deleted!");
			location.reload(true);
		},
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		error : function(jqXhr, textStatus, errorThrown) {
			return false;
		}
	});
}