$(document).ready(function() {
	$('#matchstatus').on( 'change', function() {
		$.ajax({
			url : '/zmt/hdd/matchstatusupdate?matchstatus=' + $('#matchstatus').val() + "&id=" + $('#liveid').val() ,
			type : 'get',
			headers: {
			    'X-CSRF-TOKEN' :$('#csrf').val()
			},
			success : function(data, textStatus, jQxhr) {
				location.reload();
			},
			error : function(jqXhr, textStatus, errorThrown) {
				alertify.error(errorThrown);
				return false;
			}
		});
	});
});

function update(){
	event.preventDefault();
	
	var livescore = {
		"id" : $('#liveid').val(),
		"firstBatsman" : $('#firstbatsman').val(),
		"secondBatsman" : $('#secondbatsman').val(),
		"firstBatsmanScore" : $('#firstbatsmanscore').val(),
		"secondBatsmanScore" : $('#secondbatsmanscore').val(),
		"batsmanOnStrike" : $('#batsmanonstrike').val(),
		"toss" : $('#toss').val(),
		"teamBattingFirst" : $('#teambattingfirst').val(),
		"teamBattingSecond" : $('#teambattingsecond').val(),
		"firstBowler" : $('#firstbowler').val(),
		"secondBowler" : $('#secondbowler').val(),
		"firstBowlerFigures" : $('#firstbowlerfigures').val(),
		"secondBowlerFigures" : $('#secondbowlerfigures').val(),
		"matchStatus" : $('#matchstatus').val(),
		"matchResult" : $('#matchresult').val()
	};
	
	if($('#matchstatus').val() != "Second Innings"){
		livescore.score = $('#score').val();
		livescore.wickets = $('#wickets').val();
		livescore.currentOver = $('#currentover').val();
		livescore.totalOvers = $('#totalovers').val();
	}
	
	if($('#matchstatus').val() == "Second Innings"){
		livescore.secondTeamScore = $('#score').val();
		livescore.secondTeamWickets = $('#wickets').val();
		livescore.secondTeamCurrentOver = $('#currentover').val();
		livescore.secondTeamTotalOvers = $('#totalovers').val();
	}
	
	$('#cricketlivescoreupdatebtn').prop('disabled', true);
	$('#cricketlivescoreupdatebtn').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> UPDATING');
	$.ajax({
		url : '/zmt/hdd/livecricketscoreupdate',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(livescore),
		processData : false,
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode != "OK"){
				alertify.error(data.errorMessage);
				$('#cricketlivescoreupdatebtn').prop('disabled', false);
				$('#cricketlivescoreupdatebtn').html('UPDATE');
				return false;
			}else{
				$('#cricketlivescoreupdatebtn').prop('disabled', false);
				$('#cricketlivescoreupdatebtn').html('UPDATE');
				alertify.success("Updated!");
				location.reload(true);
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error(errorThrown);
			$('#cricketlivescoreupdatebtn').prop('disabled', false);
			$('#cricketlivescoreupdatebtn').html('UPDATE');
			return false;
		}
	});
}