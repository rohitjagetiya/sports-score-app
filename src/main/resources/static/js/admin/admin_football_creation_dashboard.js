$(document).ready(function() {
	
});

var playerid;

function createFootballTeam(){
	$('#createfootballteambtn').prop('disabled', true);
	$('#createfootballteambtn').html('creating');
	var team ={
		"name": $('#teamname').val(),
		"sport": "football"
	}
	$.ajax({
		url : '/zmt/hdd/createteam',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(team),
		processData : false,
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode != "OK"){
				alertify.error(data.errorMessage);
				$('#createfootballteambtn').prop('disabled', false);
				$('#createfootballteambtn').html('create');
				return false;
			}else{
				alertify.success("Created Team!");
				$('#createfootballteambtn').prop('disabled', false);
				$('#createfootballteambtn').html('create');
				$("#teamname").val("");
				location.reload();
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error(errorThrown);
			$('#cricketlivescoreupdatebtn').prop('disabled', false);
			$('#cricketlivescoreupdatebtn').html('UPDATE');
			return false;
		}
	});
}

function createFootballSeries(){
	$('#createcfootballseriesbtn').prop('disabled', true);
	$('#createcfootballseriesbtn').html('creating');
	var team ={
		"name": $('#seriesname').val(),
		"sport": "football",
		"venue": $('#seriesvenue').val(),
		"startDate": $('#seriesstartdate').val(),
		"endDate": $('#seriesenddate').val()
	}
	$.ajax({
		url : '/zmt/hdd/createseries',
		dataType : 'json',
		type : 'post',
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		contentType : 'application/json',
		data : JSON.stringify(team),
		processData : false,
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode != "OK"){
				alertify.error(data.errorMessage);
				$('#createcfootballseriesbtn').prop('disabled', false);
				$('#createcfootballseriesbtn').html('create');
				return false;
			}else{
				alertify.success("Created Series!");
				$('#createcfootballseriesbtn').prop('disabled', false);
				$('#createcfootballseriesbtn').html('create');
				location.reload();
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error(errorThrown);
			$('#createcfootballseriesbtn').prop('disabled', false);
			$('#createcfootballseriesbtn').html('creae');
			return false;
		}
	});
}

function createFootballFixture(){
	$('#createfootballfixturebtn').prop('disabled', true);
	$('#createfootballfixturebtn').html('creating');
	var team ={
		"name": $('#fixturename').val(),
		"sport": "football",
		"venue": $('#fixturevenue').val(),
		"startDate": $('#fixturestartdate').val(),
		"series": $('#seriesselect').val(),
		"firstTeam" : $('#fixturefirstteam').val(),
		"secondTeam" : $('#fixturesecondteam').val(),
		"stadium" : $('#fixturestadium').val(),
		"date" : $('#fixturestartdate').val(),
		"time" : $('#fixturematchtime').val()
	}
	$.ajax({
		url : '/zmt/hdd/createfixture',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(team),
		processData : false,
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode != "OK"){
				alertify.error(data.errorMessage);
				$('#createfootballfixturebtn').prop('disabled', false);
				$('#createfootballfixturebtn').html('create');
				return false;
			}else{
				alertify.success("Created Fixture!");
				$('#createfootballfixturebtn').prop('disabled', false);
				$('#createfootballfixturebtn').html('create');
				location.reload();
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error(errorThrown);
			$('#createfootballfixturebtn').prop('disabled', false);
			$('#createfootballfixturebtn').html('create');
			return false;
		}
	});
}

function createfootballplayer(){
	$('#createplayerbtn').prop('disabled', true);
	$('#createplayerbtn').html('creating');
	var team ={
		"teamId": $('#teamid').val(),
		"playerName": $('#playername').val()
	}
	$.ajax({
		url : '/zmt/hdd/createplayer',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(team),
		processData : false,
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode != "OK"){
				alertify.error(data.errorMessage);
				$('#createplayerbtn').prop('disabled', false);
				$('#createplayerbtn').html('create');
				return false;
			}else{
				$('#createplayerbtn').prop('disabled', false);
				$('#createplayerbtn').html('create');
				$("#playername").val("");
				alertify.success("Created Player!");
				location.reload();
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error(errorThrown);
			$('#createplayerbtn').prop('disabled', false);
			$('#createplayerbtn').html('create');
			return false;
		}
	});
}

function delfootballplayer(id){
	$.ajax({
		url : '/zmt/hdd/deleteplayer/' + id,
		type : 'delete',
		success : function(data, textStatus, jQxhr) {
			alertify.success("Player Deleted!");
			location.reload(true);
		},
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		error : function(jqXhr, textStatus, errorThrown) {
			return false;
		}
	});
}

function setplayerid(id){
	playerid = id;
}

function updatefootballplayer(){
	$.ajax({
		url : '/zmt/hdd/updateplayer/' + playerid + '/' + $('#updateplayername').val(),
		type : 'post',
		success : function(data, textStatus, jQxhr) {
			alertify.success("Player Updated!");
			location.reload(true);
		},
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error("Could not add Player!");
			return false;
		}
	});
}

function updateteam(){
	if($('#updateteamname').val() == ""){
		alertify.error("Please enter team name");
	}
	$.ajax({
		url : '/zmt/hdd/updateteam/' + $('#teamid').val() + '/' + $('#updateteamname').val(),
		type : 'post',
		success : function(data, textStatus, jQxhr) {
			alertify.success("Team Name Updated!");
			window.location.href = "/adminfootballteams/" + $('#updateteamname').val();
		},
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error("Could not updateTeam!");
			return false;
		}
	});
}

function delteam(){
	$.ajax({
		url : '/zmt/hdd/deleteteam/' + $('#teamid').val(),
		type : 'delete',
		success : function(data, textStatus, jQxhr) {
			alertify.success("Team Deleted!");
			window.location.href = "/admindashboard";
		},
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		error : function(jqXhr, textStatus, errorThrown) {
			return false;
		}
	});
}