$(document).ready(function() {
	
});

function update(){
	event.preventDefault();
	
	var livescore = {
		"id" : $('#liveid').val(),
		"firstTeam" : $('#firstteam').val(),
		"secondTeam" : $('#secondteam').val(),
		"firstTeamScore" : $('#firstteamscore').val(),
		"secondTeamScore" : $('#secondteamscore').val(),
		"firstTeamGoals" : $('#firstteamgoals').val().trim(),
		"secondTeamGoals" : $('#secondteamgoals').val().trim(),
		"matchStatus": $('#matchstatus').val().trim()
	};
	
	$('#footballlivescoreupdatebtn').prop('disabled', true);
	$('#footballlivescoreupdatebtn').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> UPDATING');
	$.ajax({
		url : '/zmt/hdd/livefootballscoreupdate',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(livescore),
		processData : false,
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode != "OK"){
				alertify.error(data.errorMessage);
				$('#footballlivescoreupdatebtn').prop('disabled', false);
				$('#footballlivescoreupdatebtn').html('UPDATE');
				return false;
			}else{
				var scoreline = $('#firstteam').val() + ' ' + $('#firstteamscore').val() + ' - ' + $('#secondteamscore').val() + ' ' + $('#secondteam').val();
				$('#scorelinetag').text(scoreline);
				
				var firstteamgoals_array = $('#firstteamgoals').val().trim().split(" ");
				$('#firstteamgoalscorerestag').html(" ");
				for (var i = 0; i < firstteamgoals_array.length; i++) {
					if(firstteamgoals_array[i] != "")
						$('#firstteamgoalscorerestag').append('<h5 class="font-weight-bold">' + firstteamgoals_array[i].replace("-"," ") + '\' </h5>');
				}
				if(firstteamgoals_array.length == 0){
					$('#firstteamgoalscorerestag').html(" ");
				}
				
				var secondteamgoals_array = $('#secondteamgoals').val().trim().split(" ");
				$('#secondteamgoalscorerestag').html(" ");
				for (var i = 0; i < secondteamgoals_array.length; i++) {
					var item = secondteamgoals_array[i].replace("-"," ");
					if(secondteamgoals_array[i] != "")
						$('#secondteamgoalscorerestag').append('<h5 class="font-weight-bold">' + secondteamgoals_array[i].replace("-"," ") + '\' </h5>');
				}
				
				$('#footballlivescoreupdatebtn').prop('disabled', false);
				$('#footballlivescoreupdatebtn').html('UPDATE');
				alertify.success("Updated!");
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error(errorThrown);
			$('#footballlivescoreupdatebtn').prop('disabled', false);
			$('#footballlivescoreupdatebtn').html('UPDATE');
			return false;
		}
	});
}