$(document).ready(function() {
	load_data()
});

function load_data () {
	$.ajax({
		url: '/services/team/api/cricketteams',
		success: function(data) {
		}
	});
	
	$.ajax({
		url: '/services/series/api/cricketseries',
		success: function(data) {
		}
	});
}

window.setInterval(function(){
	  getlivescore();
	}, 4000);

function getlivescore(){
	$.ajax({
		url: '/services/cricketlive/api/livescore/' + $('#liveid').val(),
		success: function(data) {
			if(data.matchStatus == 'Second Innings'){
				$('#firstinningstag').hide();
				$('#secondinningstag').show();
				$('#secondteamlivescore').show();
				$('#secondinningstag').text("2nd Innings");
				$("#teambattingsecondtag").text(data.teamBattingSecond);
				$("#secondteamscoretag").text(data.secondTeamScore);
				$("#secondteamwicketstag").text(data.secondTeamWickets);
				$("#secondTeamCurrentovertag").text("(" + data.secondTeamCurrentOver + " / " + data.secondTeamTotalOvers + " overs)");
			}else{
				$('#secondteamlivescore').hide();
				$('#secondinningstag').hide();
				$('#firstinningstag').show();
				$('#firstinningstag').text("1st Innings");
				$("#scoretag").text(data.score);
				$("#wicketstag").text(data.wickets);
				$("#currentovertag").text("(" + data.currentOver + " / " + data.totalOvers + " overs)");
			}
			$("#matchresulttag").text(data.matchResult);
			$("#totaloverstag").text(data.totalOvers);
			$("#firstbatsmantag").text(data.firstBatsman + " (" + data.firstBatsmanScore + ")");
			$("#secondbatsmantag").text(data.secondBatsman + " (" + data.secondBatsmanScore + ")");
			$("#firstbowlertag").text(data.firstBowler + " (" + data.firstBowlerFigures + ")");
			$("#secondbowlertag").text(data.secondBowler + " (" + data.secondBowlerFigures + ")");
			$("#tosstag").text(data.toss);
		}
	});
}
