$(document).ready(function() {
	
	$('.body_container').click(function() {
		if(!$('.navbar-toggler').hasClass("collapsed")){
			$('.navbar-toggler').addClass("collapsed");			
			$('#navbarText').removeClass("show");
		}
	});
	
	$('#edit_file').on( 'change', function() {
	   var myfile= $( this ).val();
	   if(myfile != "")
	   {
		   var ext = myfile.split('.').pop().toLowerCase();
		   if(ext =="png" || ext =="jpeg"){
		       
			}else{
				$('#edit_file').val("");
				alertify.error("Only PNG and JPEG files can be uploaded!");
			}
	   }
	});
	
	var elements = document.getElementsByClassName("profile");
	for(var i=0; i<elements.length; i++) {
	    elements[i].style.backgroundColor = intToRGB(elements[i].innerHTML);
	}
	
	$("form").submit(function(e) {   
		e.preventDefault(); 
	});
	
	$('body')
	  .on('mouseenter mouseleave','.dropdown',toggleDropdown)
	  .on('click', '.dropdown-menu a', toggleDropdown);
	
	const scrollToTopButton = document.getElementById('js-top');

	const scrollFunc = () => {
	  let y = window.scrollY;
	  if (y > 0) {
	    $('.top-link').show();
	  } else {
	    $('.top-link').hide();
	  }
	};
	window.addEventListener("scroll", scrollFunc);
	const scrollToTop = () => {
	  const c = document.documentElement.scrollTop || document.body.scrollTop;
	  if (c > 0) {
	    window.requestAnimationFrame(scrollToTop);
	    window.scrollTo(0, c - c / 10);
	  }
	};
	scrollToTopButton.onclick = function(e) {
	  e.preventDefault();
	  scrollToTop();
	}
	
	$("#regemail").on('input', function() {
		if(!isEmail($('#regemail').val())){
			$('#regemail').removeClass("is-valid");
			$('#regemail').addClass("is-invalid");
		}else{
			$('#regemail').removeClass("is-invalid");
			$('#regemail').addClass("is-valid");
		}	
	});
	
	$("#editform").on("submit", function(e) {
		e.preventDefault();
		var data = new FormData(this);
		if ($('#edit_username').val() == "") {
			alertify.error("Please enter your Name!");
			return false;
		}
		var myfile= $('#edit_file').val();
		if(myfile != ""){
			var ext = myfile.split('.').pop().toLowerCase();
			if(ext =="png" || ext =="jpeg"){
		       
			}else{
				$('#edit_file').val("")
				alertify.error("Only PNG and JPEG files can be uploaded!");
			}
		}
		$.ajax({
			url : '/services/sportsuser/api/update',
			data: data,
		    cache: false,
		    async: false,
		    contentType: false,
		    processData: false,
		    method: 'POST',
		    type: 'POST',
			headers: {
			    'X-CSRF-TOKEN' :$('#csrf').val()
			},
		    success : function(data, textStatus, jQxhr) {
		    	if(data.responseCode == "OK"){
		    		$('#edit_file').val("");
		    		alertify.success("Successfully updated profile!");
		    		location.reload(true);
		    	}else{
		    		alertify.error(data.errorMessage);
		    	}
			},
			error : function(jqXhr, textStatus, errorThrown) {
				alertify.error("Failed to update profile!");
			}
		});
	});
});

function toggleDropdown (e) {
  const _d = $(e.target).closest('.dropdown'),
    _m = $('.dropdown-menu', _d);
  setTimeout(function(){
    const shouldOpen = e.type !== 'click' && _d.is(':hover');
    _m.toggleClass('show', shouldOpen);
    _d.toggleClass('show', shouldOpen);
    $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
  }, e.type === 'mouseleave' ? 100 : 0);
}

function verify(){
	$('#verifybtn').prop('disabled', true);
	$('#verifybtn').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> verifying');
	$('#fpemail').prop('readonly', true);
	if ($('#fpemail').val() == "" || $('#fpemail').val() == undefined || $('#fpemail').val() == null) {
		alertify.error("Please enter your Email!");
		$('#verifybtn').prop('disabled', false);
		$('#verifybtn').html('verify');
		$('#fpemail').prop('readonly', false);
		return false;
	}
	
	$.ajax({
		url : '/services/sportsuser/api/verify?id=' + $('#fpemail').val(),
		type : 'get',
		async: false,
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode == "OK"){
				$('#validate_div').show();
				$('#verifybtn').prop('disabled', true);
				$('#fpemail').prop('readonly', true);
				$('#verifybtn').html('verified');
				$('#verifybtn').removeClass('btn-secondary');
				$('#verifybtn').addClass('btn-success');
				$('#fptype').prop('disabled', true);
				start_timer();
			}
			else{
				$('#verifybtn').prop('disabled', false);
				$('#verifybtn').html('verify');
				$('#fpemail').prop('readonly', false);
				$('#validate_div').hide();
				alertify.error(data.errorMessage);
			}
		}
	});
}

function validate(){
	$('#otp').prop('readonly', true);
	$('#validatebtn').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> validating');
	$('#validatebtn').prop('disabled', true);
	if ($('#otp').val() == "" || $('#otp').val() == undefined || $('#otp').val() == null) {
		alertify.error("Please enter your OTP!");
		$('#validatebtn').html('validate');
		$('#otp').prop('readonly', false);
		$('#validatebtn').html('validate');
		$('#validatebtn').prop('disabled', false);
		return false;
	}
	$.ajax({
		url : '/services/sportsuser/api/validate?otp=' + $('#otp').val(),
		type : 'get',
		async: false,
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode == "OK"){
				$('#otp').prop('readonly', true);
				$('#validatebtn').prop('disabled', true);
				$('#validatebtn').removeClass('btn-danger');
				$('#validatebtn').addClass('btn-success');
				window.location.href = "/changepassword"
			}
			else{
				$('#otp').prop('readonly', false);
				$('#validatebtn').prop('disabled', false);
				$('#validatebtn').html('validate');
				alertify.error(data.errorMessage);
			}
		}
	});
}

function cp(){
	event.preventDefault();
	if ($('#password').val() == "") {
		alertify.error("Please enter your Password!");
		return false;
	}
	if ($('#repeat_password').val() == "") {
		alertify.error("Please re-type your Password!");
		return false;
	}
	if (($('#password').val() == $('#repeat_password').val())){
		
	}else{
		alertify.error("Passwords don't match");
		return false;	
	}
	
	var user = {
			"password" : $('#password').val()
		};
	$.ajax({
		url : '/services/sportsuser/api/changepassword',
		dataType : 'json',
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(user),
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		processData : false,
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode == "OK"){
				window.location.href = "/";
			}
			else
				alertify.error(data.errorMessage);
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error("Invalid credentials!");
		}
	});
}

function start_timer(){
	var timer2 = "4:01";
	var interval = setInterval(function() {
	  var timer = timer2.split(':');
	  //by parsing integer, I avoid all extra string processing
	  var minutes = parseInt(timer[0], 10);
	  var seconds = parseInt(timer[1], 10);
	  --seconds;
	  minutes = (seconds < 0) ? --minutes : minutes;
	  seconds = (seconds < 0) ? 59 : seconds;
	  seconds = (seconds < 10) ? '0' + seconds : seconds;
	  //minutes = (minutes < 10) ?  minutes : minutes;
	  $('.countdown').html(minutes + ':' + seconds);
	  if (minutes < 0) clearInterval(interval);
	  //check if both minutes and seconds are 0
	  if ((seconds <= 0) && (minutes <= 0)) clearInterval(interval);
	  timer2 = minutes + ':' + seconds;
	}, 1000);
}

function isEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

function register(){
	event.preventDefault();
	if ($('#name').val() == "") {
		alertify.error("Please enter your Name!");
		return false;
	}
	if ($('#userid').val() == "") {
		alertify.error("Please create a User Id!");
		return false;
	}
	if ($('#regpassword').val() == "") {
		alertify.error("Please enter your Password!");
		return false;
	}
	if ($('#repeat_password').val() == "") {
		alertify.error("Please repeat your Password!");
		return false;
	}
	if (($('#repeat_password').val() == $('#regpassword').val())){
		
	}else{
		alertify.error("Passwords don't match");
		return false;	
	}
	if ($('#regemail').val() == "") {
		alertify.error("Please enter your Email!");
		return false;
	}
	if(!isEmail($('#regemail').val())){
		alertify.error("Please enter a valid email id!");
		$('#regemail').focus();
		return false;
	}
	var sportsuser = {
			"name" : $('#name').val(),
			"userId" : $('#userid').val(),
			"email" : $('#regemail').val(),
			"password" : $('#regpassword').val()
		};
	$.ajax({
		url : '/services/sportsuser/api/register',
		dataType : 'json',
		type : 'post',
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		contentType : 'application/json',
		data : JSON.stringify(sportsuser),
		processData : false,
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode == "OK"){
				alertify.success("Successfully registered User!");
				$('#register_form').trigger("reset");
				$('#name').val("");
				$('#userid').val("");
				$('#repeat_password').val("");
				$('#address').val("");
				$('#logemail').val($('#regemail').val());
				$('#logpassword').val($('#regpassword').val());
				$('#regemail').val("");
				$('#regpassword').val("");
				location.reload(true);
			}else{
				alertify.error(data.errorMessage);
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error("Failed to register User!");
		}
	});

}

function login(){
	event.preventDefault();
	if ($('#logemail').val() == "") {
		alertify.error("Please enter your Email!");
		return false;
	}
	if ($('#logpassword').val() == "") {
		alertify.error("Please enter your Password!");
		return false;
	}
	if ($('#g-recaptcha-response').val() == "") {
		alertify.error("Please verify you're not a robot!");
		return false;
	}
	
	var user = {
			"email" : $('#logemail').val(),
			"password" : $('#logpassword').val(),
			"reCaptcha" : $('#g-recaptcha-response').val()
		};
	$.ajax({
		url : '/services/sportsuser/api/login',
		dataType : 'json',
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		type : 'post',
		contentType : 'application/json',
		data : JSON.stringify(user),
		processData : false,
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode == "OK"){
				location.reload(true);
			}
			else
				alertify.error(data.errorMessage);
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error("Invalid credentials!");
		}
	});
}

function blogit(){
	if ($('#comments').val() == "") {
		alertify.error("Please enter something to comment!");
		return false;
	}
	var data = {
		"fixtureId" : $('#fixid').val(),
		"userComments" : $('#comments').val()	
	};
	$.ajax({
		url : '/services/blog/api/blogit',
		dataType : 'json',
		type : 'post',
		headers: {
		    'X-CSRF-TOKEN' :$('#csrf').val()
		},
		contentType : 'application/json',
		data : JSON.stringify(data),
		processData : false,
		success : function(data, textStatus, jQxhr) {
			if(data.responseCode == "OK"){
				$('#comments').val("");
				alertify.success("Thanks for commenting!");
				location.reload();
			}
			else{
				alertify.error(data.errorMessage);
				$('#reglog_modal_form').modal('toggle');
			}
		},
		error : function(jqXhr, textStatus, errorThrown) {
			alertify.error("Invalid credentials!");
		}
	});
}

function logout(){
	$.ajax({
		url : '/services/sportsuser/api/logout',
		type : 'get',
		success : function(data, textStatus, jQxhr) {
			window.location.href = "/";
		}
	});
}

function logoutuser(){
	localStorage.clear();
	window.location.href = "/xfesdoienc";
}


var color,letters = '0123456789ABCDEF'.split('')
function AddDigitToColor(limit)
{
color += letters[Math.round(Math.random() * limit )]
}

function intToRGB(str){
	color = '#'
	    AddDigitToColor(5)
	    for (var i = 0; i < 5; i++) {
	        AddDigitToColor(15)
	    }
	    return color;
}