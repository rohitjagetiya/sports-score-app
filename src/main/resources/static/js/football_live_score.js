$(document).ready(function() {
	load_data()
});

function load_data () {
	$.ajax({
		url: '/services/team/api/footballteams',
		success: function(data) {
		}
	});
	
	$.ajax({
		url: '/services/series/api/footballseries',
		success: function(data) {
		}
	});
}

window.setInterval(function(){
	  getlivescore();
	}, 4000);

function getlivescore(){
	$.ajax({
		url: '/services/footballlive/api/livescore/' + $('#liveid').val(),
		success: function(data) {
			var scoreline = data.firstTeam + ' ' + data.firstTeamScore + ' - ' + data.secondTeamScore + ' ' + data.secondTeam;
			$('#scorelinetag').text(scoreline);
			
			var firstteamgoals_array = data.firstTeamGoals.trim().split(" ");
			$('#firstteamgoalscorerestag').html(" ");
			for (var i = 0; i < firstteamgoals_array.length; i++) {
				if(firstteamgoals_array[i] != "")
					$('#firstteamgoalscorerestag').append('<h5 class="font-weight-bold">' + firstteamgoals_array[i].replace("-"," ") + '\' </h5>');
			}
			if(firstteamgoals_array.length == 0){
				$('#firstteamgoalscorerestag').html(" ");
			}
			
			var secondteamgoals_array = data.secondTeamGoals.trim().split(" ");
			$('#secondteamgoalscorerestag').html(" ");
			for (var i = 0; i < secondteamgoals_array.length; i++) {
				var item = secondteamgoals_array[i].replace("-"," ");
				if(secondteamgoals_array[i] != "")
					$('#secondteamgoalscorerestag').append('<h5 class="font-weight-bold">' + secondteamgoals_array[i].replace("-"," ") + '\' </h5>');
			}
		}
	});
}
