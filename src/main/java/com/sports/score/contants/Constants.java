package com.sports.score.contants;

public class Constants {

	public static final String USER_ID_EXISTS = "User Id already exists";
	public static final String EMPTY_NAME_FIELD_ERROR = "Please enter your Name";
	public static final String EMPTY_USER_ID_FIELD_ERROR = "Please enter your User Id";
	public static final String EMPTY_PASSWORD_FIELD_ERROR = "Please enter your Password";
	public static final String EMPTY_EMAIL_FIELD_ERROR = "Please enter your Email";
	
	public static final String EMPTY_TEAM_NAME = "Please enter Team Name";
	public static final String EMPTY_SPORTS = "Sport required";
	
	public static final String SESSION_USER_EMAIL = "usrmail";
	public static final String SESSION_USER = "usr";
	public static final String SESSION_OTP_EMAIL = "otpemail";
	public static final String SESSION_OTP = "otp";
	
	public static final String LIVESCORES = "livescores";
	public static final String ERROR = "error";
	public static final String FIXTURES = "fixtures";
	public static final String THIS_SERIES = "thisseries";
	public static final String MATCH_SCORES = "matchscores";
	public static final String SERIES = "series";
	public static final String PLAYERS = "players";
	
	private Constants() {
		
	}

}
