package com.sports.score.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sports.score.models.CricketLive;
import com.sports.score.service.CricketLiveService;

/**
 * 
 * @author Rohit
 *
 *         Cricket Live API
 *
 */
@RequestMapping("/services/cricketlive/api")
@RestController
public class CricketLiveAPI {

	@Autowired
	private CricketLiveService cricketLiveService;

	/**
	 * 
	 * Get Live Cricket Score
	 * 
	 * @param request
	 * @return
	 */
	@GetMapping(path = "/livescore/{id}", produces = { "application/json" })
	public CricketLive getCricketTeams(Model model, @PathVariable(value = "id") String id) {
		return cricketLiveService.getLiveCricketMatch(Long.parseLong(id));
	}
}
