package com.sports.score.apis;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sports.score.models.Response;
import com.sports.score.service.TeamService;
import com.sports.score.util.ResponseUtil;

/**
 * 
 * @author Rohit
 *
 *         Team API
 *
 */
@RequestMapping("/services/team/api")
@RestController
public class TeamAPI {

	@Autowired
	private TeamService teamService;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	/**
	 * 
	 * Get Cricekt Teams
	 * 
	 * @param request
	 * @return
	 */
	@GetMapping(path = "/cricketteams", produces = { "application/json" })
	public Response getCricketTeams(HttpServletRequest request) {
		List<String> teams = teamService.getCricketTeams();
		return responseUtil.createSuccessResponse(teams, teams.size());
	}
	
	/**
	 * 
	 * Get Football Teams
	 * 
	 * @param request
	 * @return
	 */
	@GetMapping(path = "/footballteams", produces = { "application/json" })
	public Response getFootballTeams(HttpServletRequest request) {
		List<String> teams = teamService.getFootballTeams();
		return responseUtil.createSuccessResponse(teams, teams.size());
	}

}
