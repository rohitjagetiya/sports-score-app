package com.sports.score.apis;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sports.score.models.Response;
import com.sports.score.service.SeriesService;
import com.sports.score.util.ResponseUtil;

/**
 * 
 * @author Rohit
 *
 *         Series API
 *
 */
@RequestMapping("/services/series/api")
@RestController
public class SeriesAPI {

	@Autowired
	private SeriesService seriesService;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	/**
	 * 
	 * Get Cricket Series
	 * 
	 * @param request
	 * @return
	 */
	@GetMapping(path = "/cricketseries", produces = { "application/json" })
	public Response getCricketSeries(HttpServletRequest request) {
		List<String> series = seriesService.getAllCricketSeries();
		return responseUtil.createSuccessResponse(series, series.size());
	}

	/**
	 * 
	 * Get Football Series
	 * 
	 * @param request
	 * @return
	 */
	@GetMapping(path = "/footballseries", produces = { "application/json" })
	public Response getFootballSeries(HttpServletRequest request) {
		List<String> series = seriesService.getAllFootballSeries();
		return responseUtil.createSuccessResponse(series, series.size());
	}

}
