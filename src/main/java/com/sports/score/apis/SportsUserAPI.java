package com.sports.score.apis;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.HtmlUtils;

import com.sports.score.annotations.PasswordConstraintValidator;
import com.sports.score.contants.Constants;
import com.sports.score.models.LoginDto;
import com.sports.score.models.OTPCode;
import com.sports.score.models.Response;
import com.sports.score.models.SportsUser;
import com.sports.score.service.CaptchaService;
import com.sports.score.service.OTPService;
import com.sports.score.service.SportsUserService;
import com.sports.score.util.ResponseUtil;

/**
 * 
 * @author Rohit
 *
 *         Sports User API
 *
 */
@RequestMapping("/services/sportsuser/api")
@RestController
public class SportsUserAPI {

	private static final String BASE_PROFILE_IMAGE_PATH = System.getProperty("user.dir")
			+ "/sportsscore/profiles/images/";
	private static final Logger logger = LoggerFactory.getLogger(SportsUserAPI.class);

	@Autowired
	private SportsUserService sportsUserService;

	@Autowired
	private OTPService otpService;

	@Autowired
	private CaptchaService captchaService;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	/**
	 * Long a user with
	 * 
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@PostMapping(path = "/login", produces = { "application/json" })
	public Response login(@RequestBody LoginDto login, HttpServletRequest request) {
		if (login.getEmail() == null || ("").equalsIgnoreCase(login.getEmail().trim())) {
			return responseUtil.createFailureResponse(HttpStatus.BAD_REQUEST, "Please enter your Email!");
		}
		if (login.getPassword() == null || ("").equalsIgnoreCase(login.getPassword().trim())) {
			return responseUtil.createFailureResponse(HttpStatus.BAD_REQUEST, "Please enter your Password!");
		}
		Response captchaRsponse = captchaService.processResponse(request, login.getReCaptcha());
		if (!captchaRsponse.getResponseCode().equals(HttpStatus.OK)) {
			return captchaRsponse;
		}
		SportsUser user = new SportsUser();
		user.setEmail(HtmlUtils.htmlEscape(login.getEmail()));
		user.setPassword(HtmlUtils.htmlEscape(login.getPassword()));
		SportsUser fetchedUser = sportsUserService.login(user);
		if (fetchedUser != null) {
			request.getSession().setAttribute(Constants.SESSION_USER, fetchedUser.getName());
			request.getSession().setAttribute(Constants.SESSION_USER_EMAIL, fetchedUser.getEmail());
			return responseUtil.createOKResponse();
		}
		return responseUtil.createFailureResponse(HttpStatus.CONFLICT, "Invalid User/password");
	}

	/**
	 * Validate if a User Id already exists
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping(path = "/validateuserid", produces = { "application/json" })
	public Response validateUserId(@RequestParam(name = "userid", required = true) String userId) {
		if (sportsUserService.validateUserId(userId))
			return responseUtil.createFailureResponse(HttpStatus.CONFLICT, "User Id already exists!");
		return responseUtil.createOKResponse();
	}

	@PostMapping(path = "/register")
	public Response registerUser(@RequestBody SportsUser user, HttpServletRequest request) {
		if (user.getName() == null || user.getName().equalsIgnoreCase(""))
			return responseUtil.createFailureResponse(HttpStatus.BAD_REQUEST, "User name cannot be empty!");
		if (user.getEmail() == null || user.getEmail().equalsIgnoreCase(""))
			return responseUtil.createFailureResponse(HttpStatus.BAD_REQUEST, "User email cannot be empty!");
		if (user.getPassword() == null || user.getPassword().equalsIgnoreCase(""))
			return responseUtil.createFailureResponse(HttpStatus.BAD_REQUEST, "Password cannot be empty!");
		if (!new PasswordConstraintValidator().isValid(user.getPassword())) {
			return responseUtil.createFailureResponse(HttpStatus.BAD_REQUEST,
					"Password length must be between 8-30, 1 UpperCase, LowerCase, Digit and special character!");
		}
		if (user.getUserId() == null || user.getUserId().equalsIgnoreCase(""))
			return responseUtil.createFailureResponse(HttpStatus.BAD_REQUEST, "User Id cannot be empty!");

		if (sportsUserService.validateUserId(user.getUserId())) {
			return responseUtil.createFailureResponse(HttpStatus.CONFLICT, "User Id already exists!");
		}
		if (sportsUserService.validateEmailId(user.getEmail())) {
			return responseUtil.createFailureResponse(HttpStatus.CONFLICT,
					"Account already created with this email Id!");
		}
		
		user.setName(HtmlUtils.htmlEscape(user.getName()));
		user.setEmail(HtmlUtils.htmlEscape(user.getEmail()));
		user.setPassword(HtmlUtils.htmlEscape(user.getPassword()));
		user.setUserId(HtmlUtils.htmlEscape(user.getUserId()));
		sportsUserService.registerUser(user);
		request.getSession().setAttribute(Constants.SESSION_USER, user.getName());
		request.getSession().setAttribute(Constants.SESSION_USER_EMAIL, user.getEmail());
		return responseUtil.createOKResponse();
	}

	@PostMapping(path = "/info", produces = { "application/json" })
	public Response getUserInfo(@RequestBody LoginDto login, HttpServletRequest request) {
		SportsUser user = sportsUserService.getUserInformation(login.getEmail());
		if (user == null)
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "No such user!");
		else
			return responseUtil.createSuccessResponse(user, 1);
	}

	@PostMapping(path = "/update", produces = { "application/json" })
	public Response update(@RequestPart(value = "edit_file", required = false) MultipartFile file,
			@RequestParam(value = "edit_username") String name, HttpServletRequest request) throws IOException {
		if (name == null || ("").equalsIgnoreCase(name.trim())) {
			return responseUtil.createFailureResponse(HttpStatus.BAD_REQUEST, "Please enter your Name");
		}
		if (request.getSession() == null || request.getSession().getAttribute(Constants.SESSION_USER) == null
				|| request.getSession().getAttribute(Constants.SESSION_USER_EMAIL) == null) {
			return responseUtil.createFailureResponse(HttpStatus.UNAUTHORIZED, "Please Log In");
		}

		SportsUser user = sportsUserService
				.getUser((String) request.getSession().getAttribute(Constants.SESSION_USER_EMAIL));
		// Save resume on system
		if (!file.getOriginalFilename().isEmpty()) {
			File directory = new File(BASE_PROFILE_IMAGE_PATH);
			if (!directory.exists()) {
				directory.mkdirs();
			}
			String fileName = name.replace(" ", "_") + "_" + user.getUserId().toLowerCase() + "."
					+ FilenameUtils.getExtension(file.getOriginalFilename());
			try (BufferedOutputStream outputStream = new BufferedOutputStream(
					new FileOutputStream(new File(BASE_PROFILE_IMAGE_PATH, fileName)));) {
				outputStream.write(file.getBytes());
				outputStream.flush();
			}
			user.setProfile(fileName);
		}
		user.setName(name);
		sportsUserService.updateUser(user);
		return responseUtil.createOKResponse();
	}

	@GetMapping("/profilepic")
	@ResponseBody
	public ResponseEntity<UrlResource> serverFile(HttpServletRequest request) {
		if (request.getSession() == null || request.getSession().getAttribute(Constants.SESSION_USER) == null
				|| request.getSession().getAttribute(Constants.SESSION_USER_EMAIL) == null) {
			return ResponseEntity.badRequest().build();
		}
		SportsUser user = sportsUserService
				.getUser((String) request.getSession().getAttribute(Constants.SESSION_USER_EMAIL));
		UrlResource file = loadAsResource(user.getProfile());
		return ResponseEntity.ok().body(file);
	}

	private UrlResource loadAsResource(String filename) {
		try {
			Path file = Paths.get(BASE_PROFILE_IMAGE_PATH + "").resolve(filename);
			UrlResource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			}
		} catch (MalformedURLException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@GetMapping(path = "/logout")
	public void logout(HttpServletRequest request) {
		request.getSession().invalidate();
	}

	@GetMapping("/verify")
	public Response verifyEmailId(@RequestParam(name = "id", required = true) String email,
			HttpServletRequest request) {
		if (sportsUserService.validateEmailId(email)) {
			SportsUser user = sportsUserService.getUser(email);
			request.getSession().setAttribute(Constants.SESSION_OTP_EMAIL, email);
			return otpService.generateOTP(email, user.getUserId());
		}
		return responseUtil.createFailureResponse(HttpStatus.NON_AUTHORITATIVE_INFORMATION, "No such User!");
	}

	@GetMapping("/validate")
	public Response validateOTP(@RequestParam(name = Constants.SESSION_OTP, required = true) String otp, Model model,
			HttpServletRequest request) {
		String sessionEmail = (String) request.getSession().getAttribute(Constants.SESSION_OTP_EMAIL);
		if (sessionEmail == null) {
			return responseUtil.createFailureResponse(HttpStatus.UNAUTHORIZED, "Login required");
		}
		OTPCode otpModel = new OTPCode();
		otpModel.setUserEmail(sessionEmail);
		otpModel.setOtp(otp);
		otpModel.setDate(LocalDateTime.now());
		SportsUser user = sportsUserService.getUser(sessionEmail);
		otpModel.setUserId(user.getUserId());
		Response response = otpService.validateOTP(otpModel, request);
		if (response.getResponseCode().equals(HttpStatus.OK)) {
			request.getSession().setAttribute(Constants.SESSION_OTP, otp);
			return responseUtil.createOKResponse();
		}
		return response;
	}

	@PostMapping("/changepassword")
	public Response validateOTP(@RequestBody LoginDto login, Model model, HttpServletRequest request) {
		String sessionEmail = (String) request.getSession().getAttribute(Constants.SESSION_OTP_EMAIL);
		String otp = (String) request.getSession().getAttribute(Constants.SESSION_OTP);
		if (sessionEmail == null || otp == null) {
			return responseUtil.createFailureResponse(HttpStatus.UNAUTHORIZED, "Login required");
		}
		Response response;
		SportsUser user = new SportsUser();
		user.setPassword(login.getPassword());
		user.setEmail(sessionEmail);
		response = sportsUserService.updatePassword(user);
		if (response.getResponseCode().equals(HttpStatus.OK)) {
			SportsUser sportsUser = sportsUserService.getUser(sessionEmail);
			request.getSession().setAttribute(Constants.SESSION_USER, sportsUser.getName());
			request.getSession().setAttribute(Constants.SESSION_USER_EMAIL, sportsUser.getEmail());
		}
		request.getSession().removeAttribute(Constants.SESSION_OTP_EMAIL);
		request.getSession().removeAttribute(Constants.SESSION_OTP);
		return response;
	}

}
