package com.sports.score.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sports.score.models.FootballLive;
import com.sports.score.service.FootballLiveService;

/**
 * 
 * @author Rohit
 *
 *         Football Live API
 *
 */
@RequestMapping("/services/footballlive/api")
@RestController
public class FootballLiveAPI {

	@Autowired
	private FootballLiveService footballLiveService;

	/**
	 * 
	 * Get Live Football Score
	 * 
	 * @param request
	 * @return
	 */
	@GetMapping(path = "/livescore/{id}", produces = { "application/json" })
	public FootballLive getFootballTeams(Model model, @PathVariable(value = "id") String id) {
		return footballLiveService.getLiveFootballMatch(Long.parseLong(id));
	}
}
