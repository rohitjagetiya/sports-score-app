package com.sports.score.apis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sports.score.contants.Constants;
import com.sports.score.models.AdminUser;
import com.sports.score.models.CricketLive;
import com.sports.score.models.Fixture;
import com.sports.score.models.FootballLive;
import com.sports.score.models.Player;
import com.sports.score.models.Response;
import com.sports.score.models.Series;
import com.sports.score.models.Team;
import com.sports.score.models.TeamPlayer;
import com.sports.score.service.AdminUserService;
import com.sports.score.service.CricketLiveService;
import com.sports.score.service.FixtureService;
import com.sports.score.service.FootballLiveService;
import com.sports.score.service.PlayerService;
import com.sports.score.service.SeriesService;
import com.sports.score.service.TeamPlayerService;
import com.sports.score.service.TeamService;
import com.sports.score.util.ResponseUtil;

/**
 * 
 * @author Rohit
 *
 *         Admin REST API
 *
 */
@RequestMapping("/zmt/hdd")
@RestController
public class AdminRestAPI {

	@Autowired
	private AdminUserService adminUserService;

	@Autowired
	private CricketLiveService cricketLiveService;

	@Autowired
	private FootballLiveService footballLiveService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private SeriesService seriesService;

	@Autowired
	private FixtureService fixtureService;

	@Autowired
	private PlayerService playerService;

	@Autowired
	private TeamPlayerService teamPlayerService;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	@PostMapping(path = "/authenticateuser", consumes = { "application/json" }, produces = { "application/json" })
	public Response autheticateLogin(@RequestBody AdminUser user) {
		if (user.getName() == null || user.getName().equalsIgnoreCase(""))
			return responseUtil.createFailureResponse(HttpStatus.BAD_REQUEST, "User name cannot be empty");
		if (user.getPassword() == null || user.getPassword().equalsIgnoreCase(""))
			return responseUtil.createFailureResponse(HttpStatus.BAD_REQUEST, "Password cannot be empty");
		List<AdminUser> fetchedUser = adminUserService.findUser(user);
		if (fetchedUser.isEmpty())
			return responseUtil.createFailureResponse(HttpStatus.FORBIDDEN, "Invalid Credentials");
		return responseUtil.createOKResponse();
	}

	@PostMapping(path = "/livecricketscoreupdate", consumes = { "application/json" }, produces = { "application/json" })
	public Response updateCricketLiveScore(@RequestBody CricketLive livescore) {
		CricketLive fetchedScore = cricketLiveService.getLiveCricketMatch(livescore.getId());
		if (livescore.getToss() != null && !("").equalsIgnoreCase(livescore.getToss()))
			fetchedScore.setToss(livescore.getToss());
		if (livescore.getTeamBattingFirst() != null && !("").equalsIgnoreCase(livescore.getTeamBattingFirst()))
			fetchedScore.setTeamBattingFirst(livescore.getTeamBattingFirst());
		if (livescore.getTeamBattingSecond() != null && !("").equalsIgnoreCase(livescore.getTeamBattingSecond()))
			fetchedScore.setTeamBattingSecond(livescore.getTeamBattingSecond());
		if (livescore.getFirstBatsman() != null && !("").equalsIgnoreCase(livescore.getFirstBatsman()))
			fetchedScore.setFirstBatsman(livescore.getFirstBatsman());
		if (livescore.getSecondBatsman() != null && !("").equalsIgnoreCase(livescore.getSecondBatsman()))
			fetchedScore.setSecondBatsman(livescore.getSecondBatsman());
		if (livescore.getBatsmanOnStrike() != null && !("").equalsIgnoreCase(livescore.getBatsmanOnStrike()))
			fetchedScore.setBatsmanOnStrike(livescore.getBatsmanOnStrike());
		if (livescore.getFirstBowler() != null && !("").equalsIgnoreCase(livescore.getFirstBowler()))
			fetchedScore.setFirstBowler(livescore.getFirstBowler());
		if (livescore.getSecondBowler() != null && !("").equalsIgnoreCase(livescore.getSecondBowler()))
			fetchedScore.setSecondBowler(livescore.getSecondBowler());
		if (livescore.getFirstBowlerFigures() != null && !("").equalsIgnoreCase(livescore.getFirstBowlerFigures()))
			fetchedScore.setFirstBowlerFigures(livescore.getFirstBowlerFigures());
		if (livescore.getSecondBowlerFigures() != null && !("").equalsIgnoreCase(livescore.getSecondBowlerFigures()))
			fetchedScore.setSecondBowlerFigures(livescore.getSecondBowlerFigures());
		if (livescore.getMatchStatus() != null && !("").equalsIgnoreCase(livescore.getMatchStatus()))
			fetchedScore.setMatchStatus(livescore.getMatchStatus());
		fetchedScore.setMatchResult(livescore.getMatchResult());

		if ("Second Innings".equalsIgnoreCase(livescore.getMatchStatus())) {
			fetchedScore.setSecondTeamScore(livescore.getSecondTeamScore());
			fetchedScore.setSecondTeamWickets(livescore.getSecondTeamWickets());
			fetchedScore.setSecondTeamTotalOvers(livescore.getSecondTeamTotalOvers());
			fetchedScore.setSecondTeamCurrentOver(livescore.getSecondTeamCurrentOver());
		} else {
			fetchedScore.setScore(livescore.getScore());
			fetchedScore.setWickets(livescore.getWickets());
			fetchedScore.setTotalOvers(livescore.getTotalOvers());
			fetchedScore.setCurrentOver(livescore.getCurrentOver());
		}

		fetchedScore.setFirstBatsmanScore(livescore.getFirstBatsmanScore());
		fetchedScore.setSecondBatsmanScore(livescore.getSecondBatsmanScore());
		return cricketLiveService.save(fetchedScore);
	}

	@PostMapping(path = "/livefootballscoreupdate", consumes = { "application/json" }, produces = {
			"application/json" })
	public Response updateFootballLiveScore(@RequestBody FootballLive livescore) {
		FootballLive fetchedScore = footballLiveService.getLiveFootballMatch(livescore.getId());
		fetchedScore.setFirstTeamGoals(livescore.getFirstTeamGoals());
		fetchedScore.setSecondTeamGoals(livescore.getSecondTeamGoals());
		fetchedScore.setFirstTeamScore(livescore.getFirstTeamScore());
		fetchedScore.setSecondTeamScore(livescore.getSecondTeamScore());
		fetchedScore.setMatchStatus(livescore.getMatchStatus());
		return footballLiveService.save(fetchedScore);
	}

	@PostMapping(path = "/createteam", consumes = { "application/json" }, produces = { "application/json" })
	public Response createTeam(@RequestBody Team team) {
		if (team.getName() == null || ("").equalsIgnoreCase(team.getName())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, Constants.EMPTY_TEAM_NAME);
		}
		if (team.getSport() == null || ("").equalsIgnoreCase(team.getSport())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, Constants.EMPTY_SPORTS);
		}
		teamService.saveTeam(team);
		return responseUtil.createOKResponse();
	}

	@PostMapping(path = "/createseries", consumes = { "application/json" }, produces = { "application/json" })
	public Response createSeries(@RequestBody Series series) {
		if (series.getName() == null || ("").equalsIgnoreCase(series.getName())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, Constants.EMPTY_TEAM_NAME);
		}
		if (series.getSport() == null || ("").equalsIgnoreCase(series.getSport())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, Constants.EMPTY_SPORTS);
		}
		if (series.getVenue() == null || ("").equalsIgnoreCase(series.getVenue())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, Constants.EMPTY_SPORTS);
		}
		if (series.getStartDate() == null) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "Start Date required");
		}
		if (series.getEndDate() == null) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "End Date required");
		}
		seriesService.saveSeries(series);
		return responseUtil.createOKResponse();
	}

	@PostMapping(path = "/createfixture", consumes = { "application/json" }, produces = { "application/json" })
	public Response createFixture(@RequestBody Fixture fixture) {
		if (fixture.getName() == null || ("").equalsIgnoreCase(fixture.getName())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, Constants.EMPTY_TEAM_NAME);
		}
		if (fixture.getFirstTeam() == null || ("").equalsIgnoreCase(fixture.getFirstTeam())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "Please enter Team 1");
		}
		if (fixture.getSecondTeam() == null || ("").equalsIgnoreCase(fixture.getSecondTeam())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "Please enter Team 2");
		}
		if (fixture.getVenue() == null || ("").equalsIgnoreCase(fixture.getVenue())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "Please enter Venue");
		}
		if (fixture.getStadium() == null || ("").equalsIgnoreCase(fixture.getStadium())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "Please enter Stadium");
		}
		if (fixture.getDate() == null) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "Please enter Date");
		}
		Series series = seriesService.getSeries(fixture.getSeries(), fixture.getSport());
		if (series == null)
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "No such series!");
		fixture.setSeriesId(series.getId());
		fixtureService.saveFixture(fixture);
		return responseUtil.createOKResponse();
	}

	@DeleteMapping(path = "/deleteplayer/{id}", produces = { "application/json" })
	public Response deletePlayer(@PathVariable(value = "id") Long id) {
		teamPlayerService.deletePlayer(id);
		playerService.deletePlayer(id);
		return responseUtil.createOKResponse();
	}

	@PostMapping(path = "/updateplayer/{id}/{name}", produces = { "application/json" })
	public Response updatePlayer(@PathVariable(value = "id") Long id, @PathVariable(value = "name") String name) {
		teamPlayerService.updatePlayer(id, name);
		playerService.updatePlayer(id, name);
		return responseUtil.createOKResponse();
	}

	@PostMapping(path = "/createplayer", produces = { "application/json" })
	public Response createPlayer(@RequestBody TeamPlayer tp) {
		if (tp.getPlayerName() == null || ("").equalsIgnoreCase(tp.getPlayerName())) {
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "Please enter Player Name");
		}
		Player createdPlayer = playerService.savePlayer(tp.getPlayerName());
		teamPlayerService.savePlayer(createdPlayer.getId(), tp.getTeamId(), createdPlayer.getName());
		return responseUtil.createOKResponse();
	}

	@PostMapping(path = "/updateteam/{id}/{name}", produces = { "application/json" })
	public Response updateTeam(@PathVariable(value = "id") Long id, @PathVariable(value = "name") String name) {
		teamService.updateTeamName(id, name);
		return responseUtil.createOKResponse();
	}

	@DeleteMapping(path = "/deleteteam/{id}", produces = { "application/json" })
	public Response deleteTeam(@PathVariable(value = "id") Long id) {
		teamService.deleteTeam(id);
		return responseUtil.createOKResponse();
	}

	@DeleteMapping(path = "/deletefixture/{id}", produces = { "application/json" })
	public Response deleteFixture(@PathVariable(value = "id") Long id) {
		fixtureService.deleteFixture(id);
		return responseUtil.createOKResponse();
	}

	@GetMapping(path = "/matchstatusupdate", produces = { "application/json" })
	public Response validateUserId(@RequestParam(name = "matchstatus", required = true) String matchStatus,
			@RequestParam(name = "id", required = true) String id) {
		CricketLive fetchedScore = cricketLiveService.getLiveCricketMatch(Long.parseLong(id));
		if (fetchedScore == null)
			return responseUtil.createFailureResponse(HttpStatus.NOT_FOUND, "Invalid Match Id!");
		fetchedScore.setMatchStatus(matchStatus);
		return cricketLiveService.save(fetchedScore);
	}

}
