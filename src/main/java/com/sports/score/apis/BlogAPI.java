package com.sports.score.apis;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;

import com.sports.score.contants.Constants;
import com.sports.score.models.Blog;
import com.sports.score.models.Response;
import com.sports.score.models.SportsUser;
import com.sports.score.service.BlogService;
import com.sports.score.service.SportsUserService;
import com.sports.score.util.ResponseUtil;

/**
 * 
 * @author Rohit
 *
 *         Blog API
 *
 */
@RequestMapping("/services/blog/api")
@RestController
public class BlogAPI {

	@Autowired
	private BlogService blogService;

	@Autowired
	private SportsUserService userService;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	@PostMapping(path = "/blogit", produces = { "application/json" })
	public Response blogIt(@RequestBody Blog blog, HttpServletRequest request) {
		String user = (String) request.getSession().getAttribute(Constants.SESSION_USER);
		String email = (String) request.getSession().getAttribute(Constants.SESSION_USER_EMAIL);
		if (user == null || ("").equalsIgnoreCase(user)) {
			return responseUtil.createFailureResponse(HttpStatus.UNAUTHORIZED, "Please Login");
		}
		if (email == null || ("").equalsIgnoreCase(email)) {
			return responseUtil.createFailureResponse(HttpStatus.UNAUTHORIZED, "Please Login");
		}
		blog.setUserComments(HtmlUtils.htmlEscape(blog.getUserComments()));
		SportsUser sportsUser = userService.getUser(email);
		blog.setUserId(sportsUser.getUserId());
		return blogService.save(blog);
	}

	@GetMapping(path = "/blogit/{id}", produces = { "application/json" })
	public Response blogIt(@PathVariable(value = "id") Long fixtureId,
			@RequestParam(name = "srt", required = false) String sortBy) {
		if (sortBy == null || ("").equalsIgnoreCase(sortBy))
			return blogService.getBlogForFixtureByOldest(fixtureId);
		return blogService.getBlogForFixtureByLatest(fixtureId);
	}
}
