package com.sports.score;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sports.score.models.CricketLive;
import com.sports.score.models.Fixture;
import com.sports.score.models.FootballLive;
import com.sports.score.models.Series;
import com.sports.score.service.CricketLiveService;
import com.sports.score.service.FixtureService;
import com.sports.score.service.FootballLiveService;
import com.sports.score.service.SeriesService;

@Component
public class ScheduledTasks {

	@Autowired
	private CricketLiveService cricketLiveService;

	@Autowired
	private FootballLiveService footballLiveService;

	@Autowired
	private FixtureService fixtureService;

	@Autowired
	private SeriesService seriesService;

	/**
	 * Add Fixtures that are due to Begin in less than 24 Hours
	 */
	@Scheduled(cron = "${dailyCron}", zone = "Indian/Maldives")
	public void checkIfFixturedMatchesStartInLessThan24HoursEvery10Minutes() {
		List<Fixture> fixtures = fixtureService.getFixturesDueInLessThanADay();
		if (!fixtures.isEmpty()) {
			for (Fixture fixture : fixtures) {
				Series series = seriesService.getSeriesById(fixture.getSeriesId());
				if (series != null) {
					if (("cricket").equalsIgnoreCase(series.getSport())) {
						addFixtureToCricketLiveScoreTable(fixture);
					} else if (("football").equalsIgnoreCase(series.getSport())) {
						addFixtureToFootballLiveScoreTable(fixture);
					}
				}
			}
		}
	}

	private void addFixtureToFootballLiveScoreTable(Fixture fixture) {
		if (!footballLiveService.isFixtureAlreadyAdded(fixture.getId()).equals(Boolean.TRUE)) {
			FootballLive footballLive = new FootballLive();
			footballLive.setFixtureId(fixture.getId());
			footballLive.setName(fixture.getName());
			footballLive.setVenue(fixture.getVenue());
			footballLive.setStadium(fixture.getStadium());
			footballLive.setFirstTeam(fixture.getFirstTeam());
			footballLive.setSecondTeam(fixture.getSecondTeam());
			footballLive.setDate(fixture.getDate());
			footballLive.setTime(fixture.getTime());
			footballLive.setMatchStatus("Match yet to start");
			footballLiveService.save(footballLive);
		}
	}

	private void addFixtureToCricketLiveScoreTable(Fixture fixture) {
		if (!cricketLiveService.isFixtureAlreadyAdded(fixture.getId()).equals(Boolean.TRUE)) {
			CricketLive cricketLive = new CricketLive();
			cricketLive.setFixtureId(fixture.getId());
			cricketLive.setName(fixture.getName());
			cricketLive.setVenue(fixture.getVenue());
			cricketLive.setStadium(fixture.getStadium());
			cricketLive.setFirstTeam(fixture.getFirstTeam());
			cricketLive.setSecondTeam(fixture.getSecondTeam());
			cricketLive.setFormat(fixture.getMatchType());
			cricketLive.setDate(fixture.getDate());
			cricketLive.setTime(fixture.getTime());
			cricketLive.setMatchStatus("Match yet to start");
			cricketLiveService.save(cricketLive);
		}
	}

}
