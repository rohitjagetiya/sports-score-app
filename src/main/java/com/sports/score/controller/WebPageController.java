package com.sports.score.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.sports.score.contants.Constants;
import com.sports.score.models.CricketLive;
import com.sports.score.models.Fixture;
import com.sports.score.models.FootballLive;
import com.sports.score.models.Series;
import com.sports.score.models.Team;
import com.sports.score.service.BlogService;
import com.sports.score.service.CricketLiveService;
import com.sports.score.service.FixtureService;
import com.sports.score.service.FootballLiveService;
import com.sports.score.service.SeriesService;
import com.sports.score.service.SportsUserService;
import com.sports.score.service.TeamPlayerService;
import com.sports.score.service.TeamService;
import com.sports.score.util.ResponseUtil;

/**
 * Web-page controllers
 * 
 * @author Rohit
 * 
 */
@Controller
public class WebPageController {

	@Autowired
	private SportsUserService sportsUserService;

	@Autowired
	private SeriesService seriesService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private TeamPlayerService teamPlayerService;

	@Autowired
	private FixtureService fixtureService;

	@Autowired
	private CricketLiveService cricketLiveService;

	@Autowired
	private FootballLiveService footballLiveService;

	@Autowired
	private BlogService blogService;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	@GetMapping("/")
	public ModelAndView landingPage(Model model, HttpServletRequest request) {
		logincheck(model, request);
		return new ModelAndView("home");
	}

	@GetMapping("/about-us")
	public ModelAndView aboutUsPage(Model model, HttpServletRequest request) {
		logincheck(model, request);
		return new ModelAndView("about_us");
	}

	@GetMapping("/news-feed")
	public ModelAndView newsFeedPage(Model model, HttpServletRequest request) {
		logincheck(model, request);
		return new ModelAndView("news_feed");
	}

	@GetMapping("/changepassword")
	public ModelAndView changePasswordPage() {
		return new ModelAndView("change_password");
	}

	private void logincheck(Model model, HttpServletRequest request) {
		if (request.getSession() != null && request.getSession().getAttribute("usr") != null) {
			model.addAttribute("user", (String) request.getSession().getAttribute("usr"));
			model.addAttribute("email", (String) request.getSession().getAttribute("usrmail"));
		}
	}

	@GetMapping("/sports/cricket")
	public ModelAndView cricketHomPage(Model model, HttpServletRequest request) {
		logincheck(model, request);
		ModelAndView modelAndView = new ModelAndView();
		getBasicCricketData(model);
		List<CricketLive> liveScoreList = cricketLiveService.getLiveCricketMatches();
		model.addAttribute(Constants.LIVESCORES,
				responseUtil.createSuccessResponse(liveScoreList, liveScoreList.size()));
		modelAndView.setViewName("cricket_dashboard");
		return modelAndView;
	}

	@GetMapping("/sports/football")
	public ModelAndView footballHomePage(Model model, HttpServletRequest request) {
		logincheck(model, request);
		ModelAndView modelAndView = new ModelAndView();
		getBasicFootballData(model);
		List<FootballLive> liveScoreList = footballLiveService.getLiveFootballMatches();
		model.addAttribute(Constants.LIVESCORES,
				responseUtil.createSuccessResponse(liveScoreList, liveScoreList.size()));
		modelAndView.setViewName("football_dashboard");
		return modelAndView;
	}

	@GetMapping("/sports/cricket/team/{team}")
	public ModelAndView cricketTeamPage(Model model, @PathVariable(value = "team") String team,
			HttpServletRequest request) {
		logincheck(model, request);
		ModelAndView modelAndView = new ModelAndView();
		Team teamModel = teamService.getCricketTeam(team);
		if (teamModel == null)
			return new ModelAndView(Constants.ERROR);
		getBasicCricketData(model);
		model.addAttribute(Constants.PLAYERS, teamPlayerService.getCricketTeamPlayers(teamModel.getId()));
		model.addAttribute("team", team);
		modelAndView.setViewName("cricket_team_dashboard");
		return modelAndView;
	}

	@GetMapping("/sports/football/team/{team}")
	public ModelAndView footballTeamPage(Model model, @PathVariable(value = "team") String team,
			HttpServletRequest request) {
		logincheck(model, request);
		ModelAndView modelAndView = new ModelAndView();
		Team teamModel = teamService.getFootballTeam(team);
		if (teamModel == null)
			return new ModelAndView(Constants.ERROR);
		getBasicFootballData(model);
		model.addAttribute(Constants.PLAYERS, teamPlayerService.getFootballTeamPlayers(teamModel.getId()));
		model.addAttribute("team", team);
		modelAndView.setViewName("football_team_dashboard");
		return modelAndView;
	}

	@GetMapping("/sports/cricket/series/{series}")
	public ModelAndView cricketSeriesPage(Model model, @PathVariable(value = Constants.SERIES) String series,
			HttpServletRequest request) {
		logincheck(model, request);
		ModelAndView modelAndView = new ModelAndView();
		Series seriesModel = seriesService.getCricketSeries(series);
		if (seriesModel == null)
			return new ModelAndView(Constants.ERROR);
		getBasicCricketData(model);
		List<Fixture> fixtures = fixtureService.getFixturesOfASeries(seriesModel.getId());
		if (fixtures.isEmpty())
			model.addAttribute(Constants.FIXTURES, responseUtil.createSuccessResponse(fixtures, 0));
		else
			model.addAttribute(Constants.FIXTURES, responseUtil.createSuccessResponse(fixtures, fixtures.size()));
		model.addAttribute("thisseries", series);
		modelAndView.setViewName("cricket_series_dashboard");
		return modelAndView;
	}

	@GetMapping("/sports/football/series/{series}")
	public ModelAndView footballSeriesPage(Model model, @PathVariable(value = Constants.SERIES) String series,
			HttpServletRequest request) {
		logincheck(model, request);
		ModelAndView modelAndView = new ModelAndView();
		Series seriesModel = seriesService.getFootballSeries(series);
		if (seriesModel == null)
			return new ModelAndView(Constants.ERROR);
		getBasicFootballData(model);
		List<Fixture> fixtures = fixtureService.getFixturesOfASeries(seriesModel.getId());
		if (fixtures.isEmpty())
			model.addAttribute(Constants.FIXTURES, responseUtil.createSuccessResponse(fixtures, 0));
		else
			model.addAttribute(Constants.FIXTURES, responseUtil.createSuccessResponse(fixtures, fixtures.size()));
		model.addAttribute("thisseries", series);
		modelAndView.setViewName("football_series_dashboard");
		return modelAndView;
	}

	/**
	 * Admin Login
	 * 
	 * @return
	 */
	@GetMapping("/xfesdoienc")
	public String adminLoginPage() {
		return "admin_login";
	}

	@GetMapping("/admindashboard")
	public String adminDashboardPage(Model model) {
		getAdminBasicNavData(model);
		return "admin_dashboard";
	}

	@GetMapping("/admincricketdashboard")
	public String adminCricektDashboardPage(Model model) {
		getAdminBasicNavData(model);
		List<CricketLive> liveScoreList = cricketLiveService.getLiveCricketMatches();
		model.addAttribute(Constants.LIVESCORES,
				responseUtil.createSuccessResponse(liveScoreList, liveScoreList.size()));
		return "admin_cricket_dashboard";
	}

	@GetMapping("/admincricketdashboard/{id}")
	public String adminCricketUpdateScorePage(Model model, @PathVariable(value = "id") Long id,
			HttpServletRequest request) {
		getAdminBasicNavData(model);
		CricketLive liveScore = cricketLiveService.getLiveCricketMatch(id);
		Team teamModel = teamService.getCricketTeam(liveScore.getFirstTeam());
		model.addAttribute("firstcricketteam", teamPlayerService.getCricketTeamPlayers(teamModel.getId()));
		teamModel = teamService.getCricketTeam(liveScore.getSecondTeam());
		model.addAttribute("secondcricketteam", teamPlayerService.getCricketTeamPlayers(teamModel.getId()));
		model.addAttribute("firstteam", liveScore.getFirstTeam());
		model.addAttribute("secondteam", liveScore.getSecondTeam());
		model.addAttribute(Constants.MATCH_SCORES, responseUtil.createSuccessResponse(liveScore, 1));
		model.addAttribute("name", liveScore.getName());
		return "admin_live_cricket_score_dashboard";
	}

	@GetMapping("/adminfootballdashboard/{id}")
	public String adminFootballUpdateScorePage(Model model, @PathVariable(value = "id") Long id,
			HttpServletRequest request) {
		getAdminBasicNavData(model);
		FootballLive liveScore = footballLiveService.getLiveFootballMatch(id);
		model.addAttribute(Constants.MATCH_SCORES, responseUtil.createSuccessResponse(liveScore, 1));
		model.addAttribute("name", liveScore.getName());
		return "admin_live_football_score_dashboard";
	}

	@GetMapping("/adminfootballdashboard")
	public String adminFootballDashboardPage(Model model) {
		getAdminBasicNavData(model);
		List<FootballLive> liveScoreList = footballLiveService.getLiveFootballMatches();
		model.addAttribute(Constants.LIVESCORES,
				responseUtil.createSuccessResponse(liveScoreList, liveScoreList.size()));
		return "admin_football_dashboard";
	}

	@GetMapping("/sports/cricket/match/{id}")
	public String liveCricketScorePage(Model model, @PathVariable(value = "id") Long id, HttpServletRequest request) {
		logincheck(model, request);
		CricketLive liveScore = cricketLiveService.getLiveCricketMatch(id);
		getBasicCricketData(model);
		model.addAttribute(Constants.MATCH_SCORES, responseUtil.createSuccessResponse(liveScore, 1));
		model.addAttribute("name", liveScore.getName());
		model.addAttribute("blogs", blogService.getBlogForFixtureByLatest(liveScore.getFixtureId()));
		return "cricket_live_score";
	}

	@GetMapping("/sports/football/match/{id}")
	public String liveFootballScorePage(Model model, @PathVariable(value = "id") Long id, HttpServletRequest request) {
		logincheck(model, request);
		getBasicFootballData(model);
		FootballLive liveScore = footballLiveService.getLiveFootballMatch(id);
		model.addAttribute(Constants.MATCH_SCORES, responseUtil.createSuccessResponse(liveScore, 1));
		model.addAttribute("name", liveScore.getName());
		model.addAttribute("blogs", blogService.getBlogForFixtureByLatest(liveScore.getFixtureId()));
		return "football_live_score";
	}

	@GetMapping("/user/{name}")
	public ModelAndView userInformationPage(Model model, @PathVariable(value = "name") String name,
			HttpServletRequest request) {
		String user = (String) request.getSession().getAttribute("usr");
		String email = (String) request.getSession().getAttribute("usrmail");
		ModelAndView modelAndView = new ModelAndView();
		if (user == null || ("").equalsIgnoreCase(user)) {
			return landingPage(model, request);
		}
		if (email == null || ("").equalsIgnoreCase(email)) {
			return landingPage(model, request);
		}
		logincheck(model, request);
		model.addAttribute("userinfo", sportsUserService.getUserInformation(email));
		modelAndView.setViewName("userinfo");
		return modelAndView;
	}

	@GetMapping("/admincricketadd")
	public ModelAndView adminCricketAddTeamPage(Model model) {
		getAdminBasicNavData(model);
		ModelAndView modelAndView = new ModelAndView();
		List<String> series = seriesService.getAllCricketSeries();
		model.addAttribute(Constants.SERIES, series);
		modelAndView.setViewName("admin_cricket_add_team");
		return modelAndView;
	}

	@GetMapping("/adminfootballadd")
	public ModelAndView adminFootballAddTeamPage(Model model) {
		getAdminBasicNavData(model);
		ModelAndView modelAndView = new ModelAndView();
		List<String> series = seriesService.getAllFootballSeries();
		model.addAttribute(Constants.SERIES, series);
		modelAndView.setViewName("admin_football_add_team");
		return modelAndView;
	}

	@GetMapping("/adminfootballteams/{team}")
	public ModelAndView adminFootballTeamPage(Model model, @PathVariable(value = "team") String team,
			HttpServletRequest request) {
		getAdminBasicNavData(model);
		ModelAndView modelAndView = new ModelAndView();
		Team teamModel = teamService.getFootballTeam(team);
		if (teamModel == null)
			return new ModelAndView(Constants.ERROR);
		getBasicFootballData(model);
		model.addAttribute(Constants.PLAYERS, teamPlayerService.getFootballTeamPlayers(teamModel.getId()));
		model.addAttribute("team", team);
		model.addAttribute("teamid", teamModel.getId());
		modelAndView.setViewName("admin_football_team_dashboard");
		return modelAndView;
	}

	@GetMapping("/admincricketteams/{team}")
	public ModelAndView adminCricketTeamPage(Model model, @PathVariable(value = "team") String team,
			HttpServletRequest request) {
		getAdminBasicNavData(model);
		ModelAndView modelAndView = new ModelAndView();
		Team teamModel = teamService.getCricketTeam(team);
		if (teamModel == null)
			return new ModelAndView(Constants.ERROR);
		getBasicCricketData(model);
		model.addAttribute(Constants.PLAYERS, teamPlayerService.getCricketTeamPlayers(teamModel.getId()));
		model.addAttribute("team", team);
		model.addAttribute("teamid", teamModel.getId());
		modelAndView.setViewName("admin_cricket_team_dashboard");
		return modelAndView;
	}

	@GetMapping("/admincricketseries/{series}")
	public ModelAndView adminCricketSeriesPage(Model model, @PathVariable(value = Constants.SERIES) String series,
			HttpServletRequest request) {
		getAdminBasicNavData(model);
		logincheck(model, request);
		ModelAndView modelAndView = new ModelAndView();
		Series seriesModel = seriesService.getCricketSeries(series);
		if (seriesModel == null)
			return new ModelAndView(Constants.ERROR);
		List<Fixture> fixtures = fixtureService.getFixturesOfASeries(seriesModel.getId());
		if (fixtures.isEmpty())
			model.addAttribute(Constants.FIXTURES, responseUtil.createSuccessResponse(fixtures, 0));
		else
			model.addAttribute(Constants.FIXTURES, responseUtil.createSuccessResponse(fixtures, fixtures.size()));
		model.addAttribute("thisseries", series);
		modelAndView.setViewName("admin_cricket_series_dashboard");
		return modelAndView;
	}

	@GetMapping("/adminfootballseries/{series}")
	public ModelAndView adminFootballSeriesPage(Model model, @PathVariable(value = Constants.SERIES) String series,
			HttpServletRequest request) {
		getAdminBasicNavData(model);
		logincheck(model, request);
		ModelAndView modelAndView = new ModelAndView();
		Series seriesModel = seriesService.getFootballSeries(series);
		if (seriesModel == null)
			return new ModelAndView(Constants.ERROR);
		List<Fixture> fixtures = fixtureService.getFixturesOfASeries(seriesModel.getId());
		if (fixtures.isEmpty())
			model.addAttribute(Constants.FIXTURES, responseUtil.createSuccessResponse(fixtures, 0));
		else
			model.addAttribute(Constants.FIXTURES, responseUtil.createSuccessResponse(fixtures, fixtures.size()));
		model.addAttribute("thisseries", series);
		modelAndView.setViewName("admin_football_series_dashboard");
		return modelAndView;
	}

	private void getBasicCricketData(Model model) {
		model.addAttribute(Constants.SERIES, seriesService.getAllCricketSeries());
		model.addAttribute("teams", teamService.getCricketTeams());
	}

	private void getBasicFootballData(Model model) {
		model.addAttribute(Constants.SERIES, seriesService.getAllFootballSeries());
		model.addAttribute("teams", teamService.getFootballTeams());
	}

	private void getAdminBasicNavData(Model model) {
		model.addAttribute("footballteams", teamService.getFootballTeams());
		model.addAttribute("cricketteams", teamService.getCricketTeams());
		model.addAttribute("cricketseries", seriesService.getAllCricketSeries());
		model.addAttribute("footballseries", seriesService.getAllFootballSeries());
	}

}
