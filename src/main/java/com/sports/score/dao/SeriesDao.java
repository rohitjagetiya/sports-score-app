package com.sports.score.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sports.score.models.Series;

/**
 * 
 * Series DAO
 * 
 * @author Rohit
 *
 */
public interface SeriesDao extends JpaRepository<Series, Long> {

	@Query(value = "select name from series where sport='cricket'", nativeQuery = true)
	abstract List<String> getAllCricketSeries();

	@Query(value = "select name from series where sport='football'", nativeQuery = true)
	abstract List<String> getAllFootballSeries();

	@Query(value = "select * from series where sport='cricket' and name=:#{#series}", nativeQuery = true)
	abstract List<Series> getCricketSeries(@Param("series") String series);

	@Query(value = "select * from series where sport='football' and name=:#{#series}", nativeQuery = true)
	abstract List<Series> getFootballSeries(@Param("series") String series);
	
	@Query(value = "select * from series where sport=:#{#sport} and name=:#{#name}", nativeQuery = true)
	abstract Series getSeries(@Param("name") String name, @Param("sport") String sport);

	@Query(value = "select * from series where id=:#{#id}", nativeQuery = true)
	abstract List<Series> getSeriesById(@Param("id") Long id);

}
