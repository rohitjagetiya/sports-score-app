package com.sports.score.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sports.score.models.TeamPlayer;

/**
 * 
 * Team Player DAO
 * 
 * @author Rohit
 *
 */
public interface TeamPlayerDao extends JpaRepository<TeamPlayer, Long> {

	@Query(value = "select * from teamplayer where teamid=:#{#teamid} ORDER BY ID ASC", nativeQuery = true)
	abstract List<TeamPlayer> getCricketTeams(@Param("teamid") Long teamId);

	@Query(value = "select * from teamplayer where teamid=:#{#teamid} ORDER BY ID ASC", nativeQuery = true)
	abstract List<TeamPlayer> getFootballTeams(@Param("teamid") Long teamId);

	@Modifying
	@Transactional
	@Query(value = "delete from teamplayer where playerid=:#{#id}", nativeQuery = true)
	abstract void deletePlayer(@Param("id") Long id);

	@Modifying
	@Transactional
	@Query(value = "update teamplayer set playername=:#{#playerName} where playerid=:#{#playerId}", nativeQuery = true)
	abstract void updatePlayer(@Param("playerId") Long playerId, @Param("playerName") String playerName);

}
