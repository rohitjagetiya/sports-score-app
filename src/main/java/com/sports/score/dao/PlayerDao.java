package com.sports.score.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sports.score.models.Player;

/**
 * 
 * Player DAO
 * 
 * @author Rohit
 *
 */
public interface PlayerDao extends JpaRepository<Player, Long> {

	@Modifying
	@Transactional
	@Query(value = "delete from player where id=:#{#id}", nativeQuery = true)
	abstract void deletePlayer(@Param("id") Long id);

	@Modifying
	@Transactional
	@Query(value = "update player set name=:#{#playerName} where id=:#{#id}", nativeQuery = true)
	abstract void updatePlayer(@Param("id") Long id, @Param("playerName") String playerName);
}
