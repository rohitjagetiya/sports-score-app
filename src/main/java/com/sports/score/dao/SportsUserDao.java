package com.sports.score.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sports.score.models.SportsUser;

/**
 * 
 *  Sports User DAO
 *  
 * @author Rohit
 *
 */
public interface SportsUserDao extends JpaRepository<SportsUser, Long> {

	@Query(value = "select * from sportsuser where userid=:#{#userId}", nativeQuery = true)
	abstract List<SportsUser> findByUserId(@Param("userId") String userId);

	@Query(value = "select * from sportsuser where useremail=:#{#email}", nativeQuery = true)
	abstract List<SportsUser> findByEmail(@Param("email") String email);

	
}
