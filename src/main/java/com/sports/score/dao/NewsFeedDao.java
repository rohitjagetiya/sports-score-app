package com.sports.score.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sports.score.models.NewsFeed;

/**
 * 
 *  News Feed DAO
 *  
 * @author Rohit
 *
 */
public interface NewsFeedDao extends JpaRepository<NewsFeed, Long> {

	
}
