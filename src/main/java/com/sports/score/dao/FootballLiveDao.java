package com.sports.score.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sports.score.models.FootballLive;

/**
 * 
 * Football Live DAO
 * 
 * @author Rohit
 *
 */
public interface FootballLiveDao extends JpaRepository<FootballLive, Long> {

	@Query(value = "select * from football_live where matchstatus != 'Completed'", nativeQuery = true)
	abstract List<FootballLive> getLiveFootballMatches();

	@Query(value = "select * from football_live where id= :#{#id}", nativeQuery = true)
	abstract FootballLive getLiveFootballMatch(@Param("id") Long id);

	@Query(value = "select * from football_live where fixtureid= :#{#id}", nativeQuery = true)
	abstract List<FootballLive> isFixtureAlreadyAdded(@Param("id") Long id);

}
