package com.sports.score.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sports.score.models.Blog;

/**
 * 
 *  Blog DAO
 *  
 * @author Rohit
 *
 */
public interface BlogDao extends JpaRepository<Blog, Long> {

	@Query(value = "select * from blog WHERE fixtureid= :#{#id} order by id DESC", nativeQuery = true)
	abstract List<Blog> getBlogForFixtureByLatest(@Param("id") Long id);
	
	@Query(value = "select * from blog WHERE fixtureid= :#{#id} order by id ASC", nativeQuery = true)
	abstract List<Blog> getBlogForFixtureByOldest(@Param("id") Long id);

}
