package com.sports.score.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sports.score.models.OTPCode;

/**
 * 
 *  OTP DAO
 *  
 * @author Rohit
 *
 */
public interface OTPDao extends JpaRepository<OTPCode, Long> {

	@Query(value = "select * from otp WHERE userid= :#{#userid} and useremail= :#{#useremail} order by id DESC", nativeQuery = true)
	abstract List<OTPCode> getLatestUserRecord(@Param("userid") String userid, @Param("useremail") String useremail);

}
