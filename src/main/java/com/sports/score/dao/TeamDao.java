package com.sports.score.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sports.score.models.Team;

/**
 * 
 * Team DAO
 * 
 * @author Rohit
 *
 */
public interface TeamDao extends JpaRepository<Team, Long> {

	@Query(value = "select teamname from team where sport='cricket'", nativeQuery = true)
	abstract List<String> getCricketTeams();

	@Query(value = "select teamname from team where sport='football'", nativeQuery = true)
	abstract List<String> getFootballTeams();

	@Query(value = "select * from team where teamname=:#{#team}", nativeQuery = true)
	abstract Team getCricketTeam(@Param("team") String team);

	@Query(value = "select * from team where teamname=:#{#team}", nativeQuery = true)
	abstract Team getFootballTeam(@Param("team") String team);

	@Modifying
	@Transactional
	@Query(value = "update team set teamname=:#{#name} where id=:#{#teamId}", nativeQuery = true)
	abstract void updateTeamName(@Param("teamId") Long teamId, @Param("name") String name);

	@Modifying
	@Transactional
	@Query(value = "delete from team where id=:#{#teamId}", nativeQuery = true)
	abstract void deleteTeamById(@Param("teamId") Long teamId);

}
