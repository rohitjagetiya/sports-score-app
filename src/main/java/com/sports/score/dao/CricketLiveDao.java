package com.sports.score.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sports.score.models.CricketLive;

/**
 * 
 * Cricket Live DAO
 * 
 * @author Rohit
 *
 */
public interface CricketLiveDao extends JpaRepository<CricketLive, Long> {

	@Query(value = "select * from cricket_live where matchstatus != 'Completed'", nativeQuery = true)
	abstract List<CricketLive> getLiveCricketMatches();

	@Query(value = "select * from cricket_live where id= :#{#id}", nativeQuery = true)
	abstract CricketLive getLiveCricketMatch(@Param("id") Long id);

	@Query(value = "select * from cricket_live where fixtureid= :#{#id}", nativeQuery = true)
	abstract List<CricketLive> isFixtureAlreadyAdded(@Param("id")  Long id);

}
