package com.sports.score.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sports.score.models.Fixture;

/**
 * 
 *  Fixture DAO
 *  
 * @author Rohit
 *
 */
public interface FixtureDao extends JpaRepository<Fixture, Long> {

	@Query(value = "select * from fixture where seriesid=:#{#id} order by date ASC", nativeQuery = true)
	abstract List<Fixture> getFixtures(@Param("id") Long id);

	@Query(value = "select * from fixture WHERE date = (DATE 'tomorrow') order by date DESC", nativeQuery = true)
	abstract List<Fixture> getFixturesDueInLessThanADay();

	@Modifying
	@Transactional
	@Query(value = "delete from fixture where id=:#{#id}", nativeQuery = true)
	abstract void deleteTeamById(@Param("id") Long id);

	
}
