package com.sports.score.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sports.score.models.AdminUser;

/**
 * Admin User Dao
 * 
 * @author Rohit
 *
 */
@Repository
public interface AdminUserDao extends JpaRepository<AdminUser, Long> {

	@Query(value = "select * from adminuser where name= :#{#adminuser.name} and password= :#{#adminuser.password}", nativeQuery = true)
	abstract List<AdminUser> findUser(@Param("adminuser") AdminUser user);

	@Query(value = "select * from adminuser where name= :#{#adminuser.name}", nativeQuery = true)
	abstract List<AdminUser> findUserByName(@Param("adminuser") AdminUser user);

}
