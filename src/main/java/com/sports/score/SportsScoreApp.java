package com.sports.score;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 
 * @author Rohit
 *
 *         Main Application Class
 * 
 */
@EnableScheduling
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class SportsScoreApp {

	private static final Logger logger = LoggerFactory.getLogger(SportsScoreApp.class);

	public static void main(String[] args) {
		SpringApplication.run(SportsScoreApp.class, args);
		logger.info("Sports Score App is ready !....");
	}

}
