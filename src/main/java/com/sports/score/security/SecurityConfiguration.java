package com.sports.score.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter.XFrameOptionsMode;

/**
 * 
 * @author Rohit
 * 
 *         Security Configuration
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("rohit").password("{noop}rohit123").roles("USER", "ADMIN").and()
				.withUser("admin").password("{noop}admin@123").roles("USER", "ADMIN");
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.authorizeRequests().antMatchers("/changepassword","/favicon.ico", "/sports/**", "/xfesdoienc**", "/**").permitAll()
				.anyRequest().authenticated().and().formLogin().loginPage("/error");
		httpSecurity.headers().httpStrictTransportSecurity();
		httpSecurity.headers()
				.addHeaderWriter(new StaticHeadersWriter("X-Content-Security-Policy", "default-src 'self'"))
				.addHeaderWriter(new StaticHeadersWriter("Referrer-Policy", "no-referrer-when-downgrade"))
				.addHeaderWriter(new StaticHeadersWriter("Set-Cookie",
						"platform=desktop; Max-Age=604800; Path=/; Secure; HttpOnly"))
				.addHeaderWriter(new XFrameOptionsHeaderWriter(XFrameOptionsMode.SAMEORIGIN))
				.addHeaderWriter(new StaticHeadersWriter("X-WebKit-CSP", "default-src 'self'")).xssProtection()
				.block(true);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/favicon.ico", "/css/**", "/js/**", "/img/**", "/font/**", "/fonts/**",
				"/webfonts/**", "/services/**");
	}

}
