package com.sports.score.service;

import com.sports.score.models.Blog;
import com.sports.score.models.Response;

/**
 * Blog Service Interface
 * 
 * @author Rohit
 *
 */
public interface BlogService {

	abstract Response save(Blog blog);
	
	abstract Response getBlogForFixtureByLatest(Long id);
	
	abstract Response getBlogForFixtureByOldest(Long id);

}
