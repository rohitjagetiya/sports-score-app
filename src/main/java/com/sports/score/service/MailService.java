package com.sports.score.service;

import com.sports.score.models.OTPCode;

/**
 * 
 * @author Rohit
 * 
 *         Mail Service interface
 *
 */
public interface MailService {

	public void sendOTP(OTPCode otp);
}
