package com.sports.score.service;

import java.util.List;

import com.sports.score.models.Team;

/**
 * Team Service Interface
 * 
 * @author Rohit
 *
 */
public interface TeamService {
	
	abstract List<String> getCricketTeams();
	
	abstract List<String> getFootballTeams();

	abstract Team getCricketTeam(String team);

	abstract Team getFootballTeam(String team);

	abstract void saveTeam(Team team);

	abstract void updateTeamName(Long id, String name);

	abstract void deleteTeam(Long id);

}
