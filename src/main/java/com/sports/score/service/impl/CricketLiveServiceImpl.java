package com.sports.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports.score.dao.CricketLiveDao;
import com.sports.score.models.CricketLive;
import com.sports.score.models.Response;
import com.sports.score.service.CricketLiveService;
import com.sports.score.util.ResponseUtil;

/**
 * Cricket Live Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("cricketLiveService")
public class CricketLiveServiceImpl implements CricketLiveService {

	@Autowired
	private CricketLiveDao cricketDao;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	@Override
	public List<CricketLive> getLiveCricketMatches() {
		return cricketDao.getLiveCricketMatches();
	}

	@Override
	public CricketLive getLiveCricketMatch(Long id) {
		return cricketDao.getLiveCricketMatch(id);
	}

	@Override
	public Response save(CricketLive livescore) {
		cricketDao.save(livescore);
		return responseUtil.createOKResponse();
	}

	@Override
	public Boolean isFixtureAlreadyAdded(Long id) {
		List<CricketLive> liveMatches = cricketDao.isFixtureAlreadyAdded(id);
		return !liveMatches.isEmpty();
	}

}
