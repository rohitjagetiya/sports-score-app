package com.sports.score.service.impl;

import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.sports.score.dao.SportsUserDao;
import com.sports.score.models.Response;
import com.sports.score.models.SportsUser;
import com.sports.score.service.SportsUserService;
import com.sports.score.util.ResponseUtil;

/**
 * Sports User Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("sportsUserService")
public class SportsUserServiceImpl implements SportsUserService {

	@Autowired
	private SportsUserDao sportsUserDao;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	@Override
	public boolean validateUserId(String userId) {
		List<SportsUser> users = sportsUserDao.findByUserId(userId);
		return !users.isEmpty();
	}

	@Override
	public SportsUser login(SportsUser user) {
		List<SportsUser> users = sportsUserDao.findByEmail(user.getEmail());
		if (!users.isEmpty() && BCrypt.checkpw(user.getPassword(), users.get(0).getPassword()))
			return users.get(0);
		return null;
	}

	@Override
	public Response registerUser(SportsUser user) {
		if (validateEmailId(user.getEmail())) {
			return responseUtil.createFailureResponse(HttpStatus.CONFLICT, "User already exists");
		}
		hashPassword(user);
		sportsUserDao.save(user);
		return responseUtil.createOKResponse();
	}

	@Override
	public SportsUser getUserInformation(String email) {
		List<SportsUser> users = sportsUserDao.findByEmail(email);
		if (!users.isEmpty())
			return users.get(0);
		return null;
	}

	@Override
	public Response updatePassword(SportsUser user) {
		List<SportsUser> userList = sportsUserDao.findByEmail(user.getEmail());
		if (!userList.isEmpty()) {
			SportsUser fetchedUser = userList.get(0);
			fetchedUser.setPassword(user.getPassword());
			hashPassword(fetchedUser);
			sportsUserDao.save(fetchedUser);
			return responseUtil.createOKResponse();
		} else {
			return responseUtil.createFailureResponse(HttpStatus.NOT_FOUND, "No such User!");
		}
	}

	@Override
	public SportsUser getUser(String email) {
		List<SportsUser> userList = sportsUserDao.findByEmail(email);
		if (!userList.isEmpty()) {
			return userList.get(0);
		}
		return null;
	}

	@Override
	public void updateUser(SportsUser user) {
		sportsUserDao.save(user);
	}

	@Override
	public boolean validateEmailId(String email) {
		List<SportsUser> users = sportsUserDao.findByEmail(email);
		return !users.isEmpty();
	}

	private void hashPassword(SportsUser user) {
		user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
	}

}
