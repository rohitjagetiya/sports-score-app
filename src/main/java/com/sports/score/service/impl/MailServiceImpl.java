package com.sports.score.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.sports.score.models.Mail;
import com.sports.score.models.OTPCode;
import com.sports.score.service.MailService;

import freemarker.template.Configuration;

/**
 * Send Mail
 * 
 * @author Rohit
 *
 */
@Service("mailService")
public class MailServiceImpl implements MailService {
	
	private static final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);

	@Autowired
	private Configuration fmConfiguration;

	@Autowired
	private JavaMailSender mailSender;

	private String subject;

	private String geContentFromTemplate(Map<String, Object> model, String template) {
		StringBuilder content = new StringBuilder();
		try {
			content.append(
					FreeMarkerTemplateUtils.processTemplateIntoString(fmConfiguration.getTemplate(template), model));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return content.toString();
	}

	private void createAndSendMail(Mail mail, String template, String[] to) {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		try {
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
			mimeMessageHelper.setSubject(subject);
			mimeMessageHelper.setFrom("sportscore77@gmail.com");
			mimeMessageHelper.setTo(to);
			mail.setMailContent(geContentFromTemplate(mail.getModel(), template));
			mimeMessageHelper.setText(mail.getMailContent(), true);
			mailSender.send(mimeMessageHelper.getMimeMessage());
		} catch (MessagingException e) {
			logger.error(e.getMessage());
		}
	}

	@Override
	public void sendOTP(OTPCode otp) {
		this.subject = "Sports Score - OTP for Password Change";
		String[] receiver = { otp.getUserEmail() };
		Map<String, Object> model = new HashMap<>();
		model.put("otp", otp.getOtp());
		Mail mail = new Mail();
		mail.setModel(model);
		createAndSendMail(mail, "otp_template.txt", receiver);
	}
}
