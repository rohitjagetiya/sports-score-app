package com.sports.score.service.impl;

import java.net.URI;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.sports.score.annotations.GoogleResponse;
import com.sports.score.models.CaptchaSettings;
import com.sports.score.models.Response;
import com.sports.score.service.CaptchaService;
import com.sports.score.util.ResponseUtil;

@Service("captchaService")
public class CaptchaServiceImpl implements CaptchaService {

	@Autowired
	private CaptchaSettings captchaSettings;

	private RestTemplate restTemplate = new RestTemplate();
	
	@Autowired
	private ReCaptchaAttemptServiceImpl reCaptchaAttemptServiceImpl;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	private static final Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");

	@Override
	public Response processResponse(HttpServletRequest request, String reCaptcha) {
		String ip = request.getRemoteAddr();
		if (reCaptchaAttemptServiceImpl.isBlocked(ip)) {
			return responseUtil.createFailureResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					"Client exceeded maximum number of failed attempts");
		}
		if (!responseSanityCheck(reCaptcha)) {
			return responseUtil.createFailureResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					"Response contains invalid characters");
		}
		URI verifyUri = URI.create(
				String.format("https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s",
						captchaSettings.getSecret(), reCaptcha, ip));
		GoogleResponse googleResponse = restTemplate.getForObject(verifyUri, GoogleResponse.class);
		if (!googleResponse.isSuccess()) {
			if (googleResponse.hasClientError()) {
				reCaptchaAttemptServiceImpl.reCaptchaFailed(ip);
			}
			return responseUtil.createFailureResponse(HttpStatus.INTERNAL_SERVER_ERROR,
					"reCaptcha was not successfully validated");
		}
		reCaptchaAttemptServiceImpl.reCaptchaSucceeded(ip);
		return responseUtil.createOKResponse();
	}

	private boolean responseSanityCheck(String response) {
		return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
	}

}
