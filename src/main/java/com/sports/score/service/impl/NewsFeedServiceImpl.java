package com.sports.score.service.impl;

import org.springframework.stereotype.Service;

import com.sports.score.service.NewsFeedService;

/**
 * News Feed Implementation
 * 
 * @author Rohit
 *
 */
@Service("newsFeedService")
public class NewsFeedServiceImpl implements NewsFeedService {

}
