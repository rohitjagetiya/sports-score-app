package com.sports.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports.score.dao.PlayerDao;
import com.sports.score.models.Player;
import com.sports.score.service.PlayerService;

/**
 * Player Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("playerService")
public class PlayerServiceImpl implements PlayerService {

	@Autowired
	private PlayerDao playerDao;

	@Override
	public List<Player> getCricketTeamPlayers(String team) {
		return null;
	}

	@Override
	public List<Player> getFootballTeamPlayers(String team) {
		return null;
	}

	@Override
	public void deletePlayer(Long id) {
		playerDao.deletePlayer(id);
	}

	@Override
	public Player savePlayer(String playerName) {
		Player player = new Player();
		player.setName(playerName);
		return playerDao.save(player);
	}

	@Override
	public void updatePlayer(Long id, String name) {
		playerDao.updatePlayer(id, name);
	}

}
