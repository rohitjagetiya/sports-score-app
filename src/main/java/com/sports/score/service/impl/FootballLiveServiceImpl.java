package com.sports.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports.score.dao.FootballLiveDao;
import com.sports.score.models.FootballLive;
import com.sports.score.models.Response;
import com.sports.score.service.FootballLiveService;
import com.sports.score.util.ResponseUtil;

/**
 * Football Live Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("footballLiveService")
public class FootballLiveServiceImpl implements FootballLiveService {

	@Autowired
	private FootballLiveDao footballDao;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	@Override
	public List<FootballLive> getLiveFootballMatches() {
		return footballDao.getLiveFootballMatches();
	}

	@Override
	public FootballLive getLiveFootballMatch(Long id) {
		return footballDao.getLiveFootballMatch(id);
	}

	@Override
	public Response save(FootballLive liveScore) {
		footballDao.save(liveScore);
		return responseUtil.createOKResponse();
	}

	@Override
	public Boolean isFixtureAlreadyAdded(Long id) {
		List<FootballLive> liveMatches = footballDao.isFixtureAlreadyAdded(id);
		return !liveMatches.isEmpty();
	}

}
