package com.sports.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports.score.dao.TeamPlayerDao;
import com.sports.score.models.TeamPlayer;
import com.sports.score.service.TeamPlayerService;

/**
 * Team Player Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("teamPlayerService")
public class TeamPlayerServiceImpl implements TeamPlayerService {

	@Autowired
	private TeamPlayerDao teamPlayerDao;

	@Override
	public List<TeamPlayer> getCricketTeamPlayers(Long teamId) {
		return teamPlayerDao.getCricketTeams(teamId);
	}

	@Override
	public List<TeamPlayer> getFootballTeamPlayers(Long teamId) {
		return teamPlayerDao.getFootballTeams(teamId);
	}

	@Override
	public void deletePlayer(Long id) {
		teamPlayerDao.deletePlayer(id);
	}

	@Override
	public void savePlayer(Long playerId, Long teamId, String playerName) {
		TeamPlayer teamPlayer = new TeamPlayer();
		teamPlayer.setPlayerId(playerId);
		teamPlayer.setTeamId(teamId);
		teamPlayer.setPlayerName(playerName);
		teamPlayerDao.save(teamPlayer);

	}

	@Override
	public void updatePlayer(Long id, String name) {
		teamPlayerDao.updatePlayer(id, name);
	}

}
