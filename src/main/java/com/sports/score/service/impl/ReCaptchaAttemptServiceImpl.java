package com.sports.score.service.impl;

import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * 
 * 
 * @author RYAN
 *
 */
@Service("ReCaptchaAttemptServiceImpl")
public class ReCaptchaAttemptServiceImpl {

	private static final int MAX_ATTEMPT = 4;
	private LoadingCache<String, Integer> attemptsCache;

	public ReCaptchaAttemptServiceImpl() {
		super();
		attemptsCache = CacheBuilder.newBuilder().expireAfterWrite(4, TimeUnit.HOURS)
				.build(new CacheLoader<String, Integer>() {
					@Override
					public Integer load(String key) {
						return 0;
					}
				});
	}

	public void reCaptchaSucceeded(String key) {
		attemptsCache.invalidate(key);
	}

	public void reCaptchaFailed(String key) {
		int attempts = attemptsCache.getUnchecked(key);
		attempts++;
		attemptsCache.put(key, attempts);
	}

	public boolean isBlocked(String key) {
		return attemptsCache.getUnchecked(key) >= MAX_ATTEMPT;
	}

}
