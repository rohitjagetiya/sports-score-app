package com.sports.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.sports.score.dao.BlogDao;
import com.sports.score.models.Blog;
import com.sports.score.models.Response;
import com.sports.score.service.BlogService;
import com.sports.score.util.ResponseUtil;

/**
 * Blog Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("blogService")
public class BlogServiceImpl implements BlogService {

	@Autowired
	private BlogDao blogDao;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	@Override
	public Response save(Blog blog) {
		blogDao.save(blog);
		return responseUtil.createOKResponse();
	}

	@Override
	public Response getBlogForFixtureByLatest(Long id) {
		List<Blog> blogList = blogDao.getBlogForFixtureByLatest(id);
		if (blogList.isEmpty())
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "No Comments!");
		return responseUtil.createSuccessResponse(blogList, blogList.size());
	}

	@Override
	public Response getBlogForFixtureByOldest(Long id) {
		List<Blog> blogList = blogDao.getBlogForFixtureByOldest(id);
		if (blogList.isEmpty())
			return responseUtil.createFailureResponse(HttpStatus.NO_CONTENT, "No Comments!");
		return responseUtil.createSuccessResponse(blogList, blogList.size());
	}

}
