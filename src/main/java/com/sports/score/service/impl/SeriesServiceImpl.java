package com.sports.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports.score.dao.SeriesDao;
import com.sports.score.models.Series;
import com.sports.score.service.SeriesService;

/**
 * Series Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("seriesService")
public class SeriesServiceImpl implements SeriesService {

	@Autowired
	private SeriesDao seriesDao;

	@Override
	public List<String> getAllCricketSeries() {
		return seriesDao.getAllCricketSeries();
	}

	@Override
	public List<String> getAllFootballSeries() {
		return seriesDao.getAllFootballSeries();
	}

	@Override
	public Series getCricketSeries(String series) {
		List<Series> serieslist = seriesDao.getCricketSeries(series);
		if (serieslist.isEmpty())
			return null;
		return serieslist.get(0);
	}

	@Override
	public Series getFootballSeries(String series) {
		List<Series> serieslist = seriesDao.getFootballSeries(series);
		if (serieslist.isEmpty())
			return null;
		return serieslist.get(0);
	}

	@Override
	public void saveSeries(Series series) {
		seriesDao.save(series);
	}

	@Override
	public Series getSeries(String name, String sport) {
		return seriesDao.getSeries(name, sport);
	}

	@Override
	public Series getSeriesById(Long seriesId) {
		List<Series> series = seriesDao.getSeriesById(seriesId);
		if(!series.isEmpty())
			return series.get(0);
		return null;
	}

}
