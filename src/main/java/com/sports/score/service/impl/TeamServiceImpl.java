package com.sports.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports.score.dao.TeamDao;
import com.sports.score.models.Team;
import com.sports.score.service.TeamService;

/**
 * Team Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("teamService")
public class TeamServiceImpl implements TeamService {

	@Autowired
	private TeamDao teamDao;

	@Override
	public List<String> getCricketTeams() {
		return teamDao.getCricketTeams();
	}

	@Override
	public List<String> getFootballTeams() {
		return teamDao.getFootballTeams();
	}

	@Override
	public Team getCricketTeam(String team) {
		return teamDao.getCricketTeam(team);
	}

	@Override
	public Team getFootballTeam(String team) {
		return teamDao.getFootballTeam(team);
	}

	@Override
	public void saveTeam(Team team) {
		teamDao.save(team);
	}

	@Override
	public void updateTeamName(Long teamId, String name) {
		teamDao.updateTeamName(teamId, name);
	}

	@Override
	public void deleteTeam(Long teamId) {
		teamDao.deleteTeamById(teamId);
	}

}
