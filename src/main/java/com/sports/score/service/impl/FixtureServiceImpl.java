package com.sports.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports.score.dao.FixtureDao;
import com.sports.score.models.Fixture;
import com.sports.score.service.FixtureService;

/**
 * Fixture Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("fixtureService")
public class FixtureServiceImpl implements FixtureService {

	@Autowired
	private FixtureDao fixtureDao;

	@Override
	public List<Fixture> getFixturesOfASeries(Long id) {
		return fixtureDao.getFixtures(id);
	}

	@Override
	public void saveFixture(Fixture fixture) {
		fixtureDao.save(fixture);
	}

	@Override
	public List<Fixture> getFixturesDueInLessThanADay() {
		return fixtureDao.getFixturesDueInLessThanADay();
	}

	@Override
	public void deleteFixture(Long id) {
		fixtureDao.deleteTeamById(id);
	}

}
