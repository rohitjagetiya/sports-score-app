package com.sports.score.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sports.score.dao.AdminUserDao;
import com.sports.score.models.AdminUser;
import com.sports.score.service.AdminUserService;

/**
 * Admin User Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("adminService")
public class AdminUserServiceImpl implements AdminUserService {

	@Autowired
	private AdminUserDao adminUserDao;

	@Override
	public List<AdminUser> findUser(AdminUser user) {
		return adminUserDao.findUser(user);
	}

	@Override
	public List<AdminUser> findUserByName(AdminUser user) {
		return adminUserDao.findUserByName(user);
	}

}
