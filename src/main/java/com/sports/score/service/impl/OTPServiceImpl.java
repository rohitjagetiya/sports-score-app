package com.sports.score.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.sports.score.dao.OTPDao;
import com.sports.score.models.OTPCode;
import com.sports.score.models.Response;
import com.sports.score.service.MailService;
import com.sports.score.service.OTPService;
import com.sports.score.util.ResponseUtil;

/**
 * OTP Service Implementation
 * 
 * @author Rohit
 *
 */
@Service("otpService")
public class OTPServiceImpl implements OTPService {

	@Autowired
	private OTPDao otpDao;

	@Autowired
	private MailService mailService;

	private ResponseUtil responseUtil = ResponseUtil.getInstance();

	@Override
	public Response generateOTP(String userEmail, String userId) {
		OTPCode otp = new OTPCode();
		otp.setUserEmail(userEmail);
		otp.setUserId(userId);
		otp.setOtp(createOTP());
		otp.setDate(LocalDateTime.now());
		otpDao.save(otp);

		new Thread(new Runnable() {
			public void run() {
				mailService.sendOTP(otp);
			}
		}).start();

		return responseUtil.createOKResponse();
	}

	@Override
	public Response validateOTP(OTPCode otp, HttpServletRequest request) {
		List<OTPCode> userOtpList = otpDao.getLatestUserRecord(otp.getUserId(), otp.getUserEmail());
		LocalDateTime fourMinutesAgo = otp.getDate().minusMinutes(4);
		if (!userOtpList.isEmpty()) {
			// Date check
			if (userOtpList.get(0).getDate().isAfter(fourMinutesAgo)) {
				// OTP String check
				if (otp.getOtp().equalsIgnoreCase(userOtpList.get(0).getOtp())) {
					return responseUtil.createOKResponse();
				} else {
					return responseUtil.createFailureResponse(HttpStatus.UNAUTHORIZED, "Invalid OTP!");
				}
			} else {
				return responseUtil.createFailureResponse(HttpStatus.UNAUTHORIZED, "OTP has expired!");
			}
		}
		return responseUtil.createFailureResponse(HttpStatus.UNAUTHORIZED, "OTP has expired!");

	}

	private String createOTP() {
		return RandomStringUtils.randomAlphanumeric(6);
	}

}
