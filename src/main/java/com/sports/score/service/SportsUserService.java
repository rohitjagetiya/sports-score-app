package com.sports.score.service;

import com.sports.score.models.Response;
import com.sports.score.models.SportsUser;

/**
 * Sports User Service Interface
 * 
 * @author Rohit
 *
 */
public interface SportsUserService {

	abstract boolean validateUserId(String userId);

	abstract SportsUser login(SportsUser user);

	abstract Response registerUser(SportsUser user);

	abstract SportsUser getUserInformation(String email);

	abstract Response updatePassword(SportsUser user);

	abstract SportsUser getUser(String sessionEmail);

	abstract void updateUser(SportsUser user);

	abstract boolean validateEmailId(String email);

}
