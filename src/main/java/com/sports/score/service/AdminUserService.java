package com.sports.score.service;

import java.util.List;

import com.sports.score.models.AdminUser;

/**
 * Admin User Service Interface
 * 
 * @author Rohit
 *
 */
public interface AdminUserService {
	
	abstract List<AdminUser> findUser(AdminUser user);

	abstract List<AdminUser> findUserByName(AdminUser user);

}
