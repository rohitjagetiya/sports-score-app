package com.sports.score.service;

import java.util.List;

import com.sports.score.models.Series;

/**
 * Series Service Interface
 * 
 * @author Rohit
 *
 */
public interface SeriesService {

	abstract List<String> getAllCricketSeries();
	
	abstract List<String> getAllFootballSeries();

	abstract Series getCricketSeries(String series);
	
	abstract Series getFootballSeries(String series);
	
	abstract void saveSeries(Series series);

	abstract Series getSeries(String name, String sport);

	abstract Series getSeriesById(Long seriesId);
	
} 
