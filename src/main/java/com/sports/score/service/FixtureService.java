package com.sports.score.service;

import java.util.List;

import com.sports.score.models.Fixture;

/**
 * Fixture Service Interface
 * 
 * @author Rohit
 *
 */
public interface FixtureService {

	abstract List<Fixture> getFixturesOfASeries(Long id);
	
	abstract void saveFixture(Fixture fixture);

	abstract List<Fixture> getFixturesDueInLessThanADay();

	abstract void deleteFixture(Long id);

}
