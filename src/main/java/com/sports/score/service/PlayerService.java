package com.sports.score.service;

import java.util.List;

import com.sports.score.models.Player;

/**
 * Player Service Interface
 * 
 * @author Rohit
 *
 */
public interface PlayerService {

	abstract List<Player> getCricketTeamPlayers(String team);

	abstract List<Player> getFootballTeamPlayers(String team);

	abstract void deletePlayer(Long id);

	abstract Player savePlayer(String playerName);

	abstract void updatePlayer(Long id, String name);


}
