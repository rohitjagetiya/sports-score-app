package com.sports.score.service;

import java.util.List;

import com.sports.score.models.CricketLive;
import com.sports.score.models.Response;

/**
 * Cricket Live Service Interface
 * 
 * @author Rohit
 *
 */
public interface CricketLiveService {
	
	abstract List<CricketLive> getLiveCricketMatches();

	abstract CricketLive getLiveCricketMatch(Long id);

	abstract Response save(CricketLive livescore);

	abstract Boolean isFixtureAlreadyAdded(Long id);

}
