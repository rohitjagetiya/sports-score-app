package com.sports.score.service;

import javax.servlet.http.HttpServletRequest;

import com.sports.score.models.Response;

/**
 * Captcha Service
 * 
 * @author RYAN
 *
 */
public interface CaptchaService {

	abstract Response processResponse(HttpServletRequest request, String reCaptcha);
}
