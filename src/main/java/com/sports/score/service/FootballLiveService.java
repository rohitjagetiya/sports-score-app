package com.sports.score.service;

import java.util.List;

import com.sports.score.models.FootballLive;
import com.sports.score.models.Response;

/**
 * Football Live Service Interface
 * 
 * @author Rohit
 *
 */
public interface FootballLiveService {

	abstract List<FootballLive> getLiveFootballMatches();

	abstract FootballLive getLiveFootballMatch(Long id);

	abstract Response save(FootballLive liveScore);

	abstract Boolean isFixtureAlreadyAdded(Long id);

}
