package com.sports.score.service;

import java.util.List;

import com.sports.score.models.TeamPlayer;

/**
 * Team Player Service Interface
 * 
 * @author Rohit
 *
 */
public interface TeamPlayerService {

	abstract List<TeamPlayer> getCricketTeamPlayers(Long teamId);

	abstract List<TeamPlayer> getFootballTeamPlayers(Long teamId);

	abstract void deletePlayer(Long id);

	abstract void savePlayer(Long playerId, Long teamId, String playerName);

	abstract void updatePlayer(Long id, String name);

}
