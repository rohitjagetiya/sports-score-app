package com.sports.score.service;

import javax.servlet.http.HttpServletRequest;

import com.sports.score.models.OTPCode;
import com.sports.score.models.Response;

/**
 * OTP Service Interface
 * 
 * @author Rohit
 *
 */
public interface OTPService {

	abstract Response generateOTP(String userEmail, String userId);

	abstract Response validateOTP(OTPCode otp, HttpServletRequest request);

}
