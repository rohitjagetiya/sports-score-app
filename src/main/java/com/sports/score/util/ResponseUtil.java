package com.sports.score.util;

import org.springframework.http.HttpStatus;

import com.sports.score.models.Response;

/**
 * 
 * @author Rohit
 *
 *         Sports Score Response Utility
 *
 */
public class ResponseUtil {

	private static ResponseUtil responseUtil = null;

	private ResponseUtil() {

	}

	public static ResponseUtil getInstance() {
		if (responseUtil == null)
			responseUtil = new ResponseUtil();
		return responseUtil;
	}

	public Response createSuccessResponse(Object data, int size) {
		Response response = new Response();
		response.setResponseCode(HttpStatus.OK);
		response.setData(data);
		response.setCount(size);
		return response;
	}

	public Response createOKResponse() {
		Response response = new Response();
		response.setResponseCode(HttpStatus.OK);
		return response;
	}

	public Response createFailureResponse(HttpStatus httpStatus, String errorMessage) {
		Response response = new Response();
		response.setResponseCode(httpStatus);
		response.setErrorMessage(errorMessage);
		return response;
	}

}
