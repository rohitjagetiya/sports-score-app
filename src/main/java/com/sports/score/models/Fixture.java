package com.sports.score.models;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * Fixture Model
 * 
 * @author Rohit
 *
 */
@Entity
@Table(name = "fixture")
public class Fixture implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name")
	private String name;
	@Column(name = "matchtype")
	private String matchType;
	@Column(name = "firstteam")
	private String firstTeam;
	@Column(name = "secondteam")
	private String secondTeam;
	@Column(name = "venue")
	private String venue;
	@Column(name = "stadium")
	private String stadium;
	@Column(name = "date")
	private Date date;
	@Column(name = "time")
	private LocalTime time;
	@Column(name = "seriesid")
	private Long seriesId;
	@Transient
	private String sport;
	@Transient
	private String series;

	public Fixture() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMatchType() {
		return matchType;
	}

	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}

	public String getFirstTeam() {
		return firstTeam;
	}

	public void setFirstTeam(String firstTeam) {
		this.firstTeam = firstTeam;
	}

	public String getSecondTeam() {
		return secondTeam;
	}

	public void setSecondTeam(String secondTeam) {
		this.secondTeam = secondTeam;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getStadium() {
		return stadium;
	}

	public void setStadium(String stadium) {
		this.stadium = stadium;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public Long getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(Long seriesId) {
		this.seriesId = seriesId;
	}

	public String getSport() {
		return sport;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	@Override
	public String toString() {
		return "Fixture [id=" + id + ", name=" + name + ", matchType=" + matchType + ", firstTeam=" + firstTeam
				+ ", secondTeam=" + secondTeam + ", venue=" + venue + ", stadium=" + stadium + ", date=" + date
				+ ", time=" + time + ", seriesId=" + seriesId + ", sport=" + sport + ", series=" + series + "]";
	}

}
