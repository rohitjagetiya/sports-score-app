package com.sports.score.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Team Player User Model
 * 
 * @author Rohit
 *
 */
@Entity
@Table(name = "teamplayer")
public class TeamPlayer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "teamid")
	private Long teamId;
	@Column(name = "playerid")
	private Long playerId;
	@Column(name = "playername")
	private String playerName;

	public TeamPlayer() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	@Override
	public String toString() {
		return "TeamPlayer [id=" + id + ", teamId=" + teamId + ", playerId=" + playerId + ", playerName=" + playerName
				+ "]";
	}

}
