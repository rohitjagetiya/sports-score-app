package com.sports.score.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * News Feed Model
 * 
 * @author Rohit
 *
 */
@Entity
@Table(name = "newsfeed")
public class NewsFeed implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "title")
	private String title;
	@Column(name = "srticle")
	private String article;
	@Column(name = "username")
	private String userName;
	@Column(name = "posteddate")
	private LocalDateTime postedDate;

	public NewsFeed() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public LocalDateTime getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(LocalDateTime postedDate) {
		this.postedDate = postedDate;
	}

	@Override
	public String toString() {
		return "NewsFeed [id=" + id + ", title=" + title + ", article=" + article + ", userName=" + userName
				+ ", postedDate=" + postedDate + "]";
	}

}
