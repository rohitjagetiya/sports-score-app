package com.sports.score.models;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Cricket Live Model
 * 
 * @author Rohit
 *
 */
@Entity
@Table(name = "cricket_live")
public class CricketLive implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "fixtureid")
	private Long fixtureId;
	@Column(name = "score")
	private Long score;
	@Column(name = "wickets")
	private Long wickets;
	@Column(name = "name")
	private String name;
	@Column(name = "format")
	private String format;
	@Column(name = "firstteam")
	private String firstTeam;
	@Column(name = "secondteam")
	private String secondTeam;
	@Column(name = "venue")
	private String venue;
	@Column(name = "stadium")
	private String stadium;
	@Column(name = "date")
	private Date date;
	@Column(name = "time")
	private LocalTime time;
	@Column(name = "matchstatus")
	private String matchStatus;
	@Column(name = "firstbatsman")
	private String firstBatsman;
	@Column(name = "secondbatsman")
	private String secondBatsman;
	@Column(name = "firstbowler")
	private String firstBowler;
	@Column(name = "secondbowler")
	private String secondBowler;
	@Column(name = "firstbowlerfigures")
	private String firstBowlerFigures;
	@Column(name = "secondbowlerfigures")
	private String secondBowlerFigures;
	@Column(name = "firstbatsmanscore")
	private Long firstBatsmanScore;
	@Column(name = "secondbatsmanscore")
	private Long secondBatsmanScore;
	@Column(name = "currentover")
	private String currentOver;
	@Column(name = "totalovers")
	private String totalOvers;
	@Column(name = "batsmanonstrike")
	private String batsmanOnStrike;
	@Column(name = "teambattingfirst")
	private String teamBattingFirst;
	@Column(name = "teambattingsecond")
	private String teamBattingSecond;
	@Column(name = "toss")
	private String toss;

	@Column(name = "matchresult")
	private String matchResult;
	@Column(name = "secondteamscore")
	private Long secondTeamScore;
	@Column(name = "secondteamwickets")
	private Long secondTeamWickets;
	@Column(name = "secondteamcurrentover")
	private String secondTeamCurrentOver;
	@Column(name = "secondteamtotalovers")
	private String secondTeamTotalOvers;

	public CricketLive() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFixtureId() {
		return fixtureId;
	}

	public void setFixtureId(Long fixtureId) {
		this.fixtureId = fixtureId;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Long getWickets() {
		return wickets;
	}

	public void setWickets(Long wickets) {
		this.wickets = wickets;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getFirstTeam() {
		return firstTeam;
	}

	public void setFirstTeam(String firstTeam) {
		this.firstTeam = firstTeam;
	}

	public String getSecondTeam() {
		return secondTeam;
	}

	public void setSecondTeam(String secondTeam) {
		this.secondTeam = secondTeam;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getStadium() {
		return stadium;
	}

	public void setStadium(String stadium) {
		this.stadium = stadium;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public String getMatchStatus() {
		return matchStatus;
	}

	public void setMatchStatus(String matchStatus) {
		this.matchStatus = matchStatus;
	}

	public String getFirstBatsman() {
		return firstBatsman;
	}

	public void setFirstBatsman(String firstBatsman) {
		this.firstBatsman = firstBatsman;
	}

	public String getSecondBatsman() {
		return secondBatsman;
	}

	public void setSecondBatsman(String secondBatsman) {
		this.secondBatsman = secondBatsman;
	}

	public Long getFirstBatsmanScore() {
		return firstBatsmanScore;
	}

	public void setFirstBatsmanScore(Long firstBatsmanScore) {
		this.firstBatsmanScore = firstBatsmanScore;
	}

	public Long getSecondBatsmanScore() {
		return secondBatsmanScore;
	}

	public void setSecondBatsmanScore(Long secondBatsmanScore) {
		this.secondBatsmanScore = secondBatsmanScore;
	}

	public String getCurrentOver() {
		return currentOver;
	}

	public void setCurrentOver(String currentOver) {
		this.currentOver = currentOver;
	}

	public String getTotalOvers() {
		return totalOvers;
	}

	public void setTotalOvers(String totalOvers) {
		this.totalOvers = totalOvers;
	}

	public String getBatsmanOnStrike() {
		return batsmanOnStrike;
	}

	public void setBatsmanOnStrike(String batsmanOnStrike) {
		this.batsmanOnStrike = batsmanOnStrike;
	}

	public String getTeamBattingFirst() {
		return teamBattingFirst;
	}

	public void setTeamBattingFirst(String teamBattingFirst) {
		this.teamBattingFirst = teamBattingFirst;
	}

	public String getTeamBattingSecond() {
		return teamBattingSecond;
	}

	public void setTeamBattingSecond(String teamBattingSecond) {
		this.teamBattingSecond = teamBattingSecond;
	}

	public String getToss() {
		return toss;
	}

	public void setToss(String toss) {
		this.toss = toss;
	}

	public String getFirstBowler() {
		return firstBowler;
	}

	public void setFirstBowler(String firstBowler) {
		this.firstBowler = firstBowler;
	}

	public String getSecondBowler() {
		return secondBowler;
	}

	public void setSecondBowler(String secondBowler) {
		this.secondBowler = secondBowler;
	}

	public String getFirstBowlerFigures() {
		return firstBowlerFigures;
	}

	public void setFirstBowlerFigures(String firstBowlerFigures) {
		this.firstBowlerFigures = firstBowlerFigures;
	}

	public String getSecondBowlerFigures() {
		return secondBowlerFigures;
	}

	public void setSecondBowlerFigures(String secondBowlerFigures) {
		this.secondBowlerFigures = secondBowlerFigures;
	}

	public String getMatchResult() {
		return matchResult;
	}

	public void setMatchResult(String matchResult) {
		this.matchResult = matchResult;
	}

	public Long getSecondTeamScore() {
		return secondTeamScore;
	}

	public void setSecondTeamScore(Long secondTeamScore) {
		this.secondTeamScore = secondTeamScore;
	}

	public Long getSecondTeamWickets() {
		return secondTeamWickets;
	}

	public void setSecondTeamWickets(Long secondTeamWickets) {
		this.secondTeamWickets = secondTeamWickets;
	}

	public String getSecondTeamCurrentOver() {
		return secondTeamCurrentOver;
	}

	public void setSecondTeamCurrentOver(String secondTeamCurrentOver) {
		this.secondTeamCurrentOver = secondTeamCurrentOver;
	}

	public String getSecondTeamTotalOvers() {
		return secondTeamTotalOvers;
	}

	public void setSecondTeamTotalOvers(String secondTeamTotalOvers) {
		this.secondTeamTotalOvers = secondTeamTotalOvers;
	}

	@Override
	public String toString() {
		return "CricketLive [id=" + id + ", fixtureId=" + fixtureId + ", score=" + score + ", wickets=" + wickets
				+ ", name=" + name + ", format=" + format + ", firstTeam=" + firstTeam + ", secondTeam=" + secondTeam
				+ ", venue=" + venue + ", stadium=" + stadium + ", date=" + date + ", time=" + time + ", matchStatus="
				+ matchStatus + ", firstBatsman=" + firstBatsman + ", secondBatsman=" + secondBatsman + ", firstBowler="
				+ firstBowler + ", secondBowler=" + secondBowler + ", firstBowlerFigures=" + firstBowlerFigures
				+ ", secondBowlerFigures=" + secondBowlerFigures + ", firstBatsmanScore=" + firstBatsmanScore
				+ ", secondBatsmanScore=" + secondBatsmanScore + ", currentOver=" + currentOver + ", totalOvers="
				+ totalOvers + ", batsmanOnStrike=" + batsmanOnStrike + ", teamBattingFirst=" + teamBattingFirst
				+ ", teamBattingSecond=" + teamBattingSecond + ", toss=" + toss + ", matchResult=" + matchResult
				+ ", secondTeamScore=" + secondTeamScore + ", secondTeamWickets=" + secondTeamWickets
				+ ", secondTeamCurrentOver=" + secondTeamCurrentOver + ", secondTeamTotalOvers=" + secondTeamTotalOvers
				+ "]";
	}

}
