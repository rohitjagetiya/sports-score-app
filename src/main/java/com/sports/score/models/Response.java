package com.sports.score.models;

import org.springframework.http.HttpStatus;

/**
 * API Response Builder Class
 * 
 * @author Rohit
 *
 */
public class Response {

	private HttpStatus responseCode;
	private String errorMessage;
	private int count;
	private long totalRecords;
	private int totalPages;
	private Object data;
	private int currentSlice;

	public Response() {
		super();
	}

	public HttpStatus getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(HttpStatus responseCode) {
		this.responseCode = responseCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getCurrentSlice() {
		return currentSlice;
	}

	public void setCurrentSlice(int currentSlice) {
		this.currentSlice = currentSlice;
	}

	@Override
	public String toString() {
		return "Response [responseCode=" + responseCode + ", errorMessage=" + errorMessage + ", count=" + count
				+ ", totalRecords=" + totalRecords + ", totalPages=" + totalPages + ", data=" + data + ", currentSlice="
				+ currentSlice + "]";
	}

}
