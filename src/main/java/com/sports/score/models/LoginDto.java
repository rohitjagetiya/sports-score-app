package com.sports.score.models;

import java.io.Serializable;

/**
 * 
 * Login Page DTO
 * 
 * @author Rohit
 *
 */
public class LoginDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String email;
	private String password;
	private String reCaptcha;

	public LoginDto() {
		super();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getReCaptcha() {
		return reCaptcha;
	}

	public void setReCaptcha(String reCaptcha) {
		this.reCaptcha = reCaptcha;
	}

	@Override
	public String toString() {
		return "LoginDto [email=" + email + ", password=" + password + "]";
	}

}
