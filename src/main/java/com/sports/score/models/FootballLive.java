package com.sports.score.models;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Football Live Model
 * 
 * @author Rohit
 *
 */
@Entity
@Table(name = "football_live")
public class FootballLive implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "fixtureid")
	private Long fixtureId;
	@Column(name = "name")
	private String name;
	@Column(name = "firstteam")
	private String firstTeam;
	@Column(name = "secondteam")
	private String secondTeam;
	@Column(name = "venue")
	private String venue;
	@Column(name = "stadium")
	private String stadium;
	@Column(name = "date")
	private Date date;
	@Column(name = "time")
	private LocalTime time;
	@Column(name = "matchstatus")
	private String matchStatus;
	@Column(name = "firstteamgoals")
	private String firstTeamGoals;
	@Column(name = "secondteamgoals")
	private String secondTeamGoals;
	@Column(name = "firstteamscore")
	private Long firstTeamScore;
	@Column(name = "secondteamscore")
	private Long secondTeamScore;

	public FootballLive() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFixtureId() {
		return fixtureId;
	}

	public void setFixtureId(Long fixtureId) {
		this.fixtureId = fixtureId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstTeam() {
		return firstTeam;
	}

	public void setFirstTeam(String firstTeam) {
		this.firstTeam = firstTeam;
	}

	public String getSecondTeam() {
		return secondTeam;
	}

	public void setSecondTeam(String secondTeam) {
		this.secondTeam = secondTeam;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getStadium() {
		return stadium;
	}

	public void setStadium(String stadium) {
		this.stadium = stadium;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public String getMatchStatus() {
		return matchStatus;
	}

	public void setMatchStatus(String matchStatus) {
		this.matchStatus = matchStatus;
	}

	public String getFirstTeamGoals() {
		return firstTeamGoals;
	}

	public void setFirstTeamGoals(String firstTeamGoals) {
		this.firstTeamGoals = firstTeamGoals;
	}

	public String getSecondTeamGoals() {
		return secondTeamGoals;
	}

	public void setSecondTeamGoals(String secondTeamGoals) {
		this.secondTeamGoals = secondTeamGoals;
	}

	public Long getFirstTeamScore() {
		return firstTeamScore;
	}

	public void setFirstTeamScore(Long firstTeamScore) {
		this.firstTeamScore = firstTeamScore;
	}

	public Long getSecondTeamScore() {
		return secondTeamScore;
	}

	public void setSecondTeamScore(Long secondTeamScore) {
		this.secondTeamScore = secondTeamScore;
	}

	@Override
	public String toString() {
		return "FootballLive [id=" + id + ", fixtureId=" + fixtureId + ", name=" + name + ", firstTeam=" + firstTeam
				+ ", secondTeam=" + secondTeam + ", venue=" + venue + ", stadium=" + stadium + ", date=" + date
				+ ", time=" + time + ", matchStatus=" + matchStatus + ", firstTeamGoals=" + firstTeamGoals
				+ ", secondTeamGoals=" + secondTeamGoals + ", firstTeamScore=" + firstTeamScore + ", secondTeamScore="
				+ secondTeamScore + "]";
	}

}
