package com.sports.score.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Sports User Model
 * 
 * @author Rohit
 *
 */
@Entity
@Table(name = "sportsuser")
public class SportsUser implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "userid")
	private String userId;
	@Column(name = "useremail")
	private String email;
	@Column(name = "username")
	private String name;
	@Column(name = "password")
	private String password;
	@Column(name = "blacklisted")
	private Boolean blacklisted;
	@Column(name = "profile")
	private String profile;

	public SportsUser() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getBlacklisted() {
		return blacklisted;
	}

	public void setBlacklisted(Boolean blacklisted) {
		this.blacklisted = blacklisted;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	@Override
	public String toString() {
		return "SportsUser [id=" + id + ", userId=" + userId + ", email=" + email + ", name=" + name + ", password="
				+ password + ", blacklisted=" + blacklisted + ", profile=" + profile + "]";
	}

}
